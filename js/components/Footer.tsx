import React from 'react';
import {FB_ICON, INST_ICON, MAIN_LOGO, OK_ICON, TW_ICON, VK_ICON} from "../constants/IconConstants";
import Link from "next/link";
import SiteMenu from "./pieces/SiteMenu";
import "../../scss/components/footer.scss";
import Button from "./pieces/Button";

export default function Footer() {
    // const [showInfo, changePosition] = useState(1);
    // useEffect(() => {
    //     let width = window.innerWidth;
    //     if (width >= 1024) {
    //         changePosition(2);
    //     }
    // });
    const info = () => {
        return (
            <div className="footer__block-info">
                <p className="footer__block-info-text">
                    © 2011-2019
                    Общество с ограниченной ответственностью &laquo;Микрокредитная компания Скорость Финанс&raquo;
                </p>
                <p className="footer__block-info-text">
                    Настоящий сайт принадлежит и управляется ООО «МКК СКОРФИН» (ИНН 3664223480, ОГРН 1163668109428).
                    Общество
                    состоит в государственном реестре микрофинансовых организаций ЦБ РФ с 27.04.2017 года.
                    Регистрационный номер
                    записи в государственном реестре микрофинансовых организаций 1703020008232. Общество ведет
                    свою деятельность на
                    территории и в соответствии с законодательством Российской Федерации.
                </p>
                <p className="footer__block-info-text">
                    Онлайн-микрозаймы предоставляет ООО «МФК НОВОЕ ФИНАНСИРОВАНИЕ» (ИНН 6162073437, ОГРН 1166196099057).
                    Общество
                    состоит в государственном реестре микрофинансовых организаций ЦБ РФ с 22.12.2016 года.
                    Регистрационный номер
                    записи в государственном реестре микрофинансовых организаций 1603760008057. Компания ведет свою
                    деятельность на
                    территории и в соответствии с законодательством Российской Федерации*.
                </p>
                <p className="footer__block-info-text">
                    * Предоставление онлайн-микрозаймов на сайте www.dengisrazy.online осуществляется ООО «МФК НОВОЕ
                    ФИНАНСИРОВАНИЕ»
                    в рамках партнерской программы, действующей в соответствии с соглашением, заключённым между ООО «МКК
                    СКОРФИН» и
                    ООО «МФК НОВОЕ ФИНАНСИРОВАНИЕ

                </p>
            </div>
        )
    };
    return (
        <footer className="footer">

            <div className="footer__wrapper">
                <div className="footer__block">
                    <p className="footer__logo-wrapper">
                        <Link href="index">
                            <a>{MAIN_LOGO}
                                {/*index*/}
                            </a>
                        </Link>
                    </p>
                    {/*{showInfo === 2 ? info() : ""}*/}
                    <ul className="footer__social-list">
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {VK_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {FB_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {OK_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {INST_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {TW_ICON}
                            </a>
                        </li>
                    </ul>
                </div>
                <SiteMenu
                    mainBlock="footer"
                />
                {/*<a*/}
                {/*    href="https://finbridge.io/?utm_source=ds"*/}
                {/*    className="footer__link-out"*/}
                {/*>Инвесторам</a>*/}

            </div>
            {/*{showInfo === 1 ? info() : ""}*/}
            {info()}

        </footer>
    )
}



