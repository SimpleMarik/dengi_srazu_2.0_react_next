import React from "react";
import {MAIN_LOGO, PHONE_ICON, USER_ICON, LC_ICON} from "../constants/IconConstants";
// import HeaderMenu from "./HeaderMenu";
// import Button from "./pieces/Button.jsx";
import cookie from 'react-cookies';
import HeaderMenu from "./HeaderMenu";
import "../../scss/components/header.scss";
import Link from "next/link";
import Router from "next/router";

// interface events {
//     onClick: any,
//     _setOpenMenu: any,
//     base: any,
//     header: any,
//     isOpenMenu: any,
//     supportPhone: any,
//     removeEventListener: any,
//     dispatchEvent: any,
//     fistName: string
//
// }


export default function Header(props) {
    const renderMenuBtn = () => {
        return (
            <div className="header-menu__btn-wrapper">
                <button
                    className={isOpenMenu ? "header-menu__btn header-menu__btn--active" : "header-menu__btn header-menu__btn--anim"}
                    onClick={props._setOpenMenu}

                >
                        <span>
                            {/*btn*/}
                        </span>
                </button>
            </div>
        )
    };
    const isOpenMenu = props.header.isOpenMenu;
    const cook = cookie.load("Auth");

    // const name =cook ? cook.lastName + " " + cook.firstName.slice(0,1) + ". " + cook.middleName.slice(0,1) + "." : "";
    const name = "";
    let path;
    if (process.browser) {
        path = Router.asPath;
        }

    return (
        <header className="header">
            <div className="header__wrapper">
                <p className="header__logo-wrapper">
                    <Link href="/">
                        {MAIN_LOGO}
                    </Link>
                </p>
                {/*{props.goTest ? <Button*/}
                {/*    onClick={props.test}*/}
                {/*    text="TEST"*/}
                {/*/> : ""}*/}
                <div className="header__action-wrapper">
                    {cook ?
                        <div className="header__user-wrapper">
                            {USER_ICON}
                            <p className="header__user">
                                {name}
                            </p>
                        </div> :
                        <div className="header__phone-wrapper">
                            {PHONE_ICON}
                            <div className="header__phone">
                                {/*{props.base.supportPhone}*/}
                                <a href={"tel:88007007424"}
                                   // className="header-menu__contact-telephone"
                                >
                                    8 (800) 700 74 24
                                </a>
                                <a href={"tel:88007004348"}
                                   // className="header-menu__contact-telephone"
                                >
                                    8 (800) 700 43 48
                                </a>
                            </div>
                        </div>

                    }
                    <div className="header__pa-link-wrapper">
                        {/*<Link href="/cabinet">*/}
                        <Link href="/login">
                            <a className="header__pa-link">{LC_ICON}
                                Личный кабинет</a>
                        </Link>
                    </div>
                    {path !== "/cabinet" ? renderMenuBtn() : ""}
                </div>
            </div>
            {isOpenMenu ? <HeaderMenu
                supportPhone={props.header.supportPhone}
                _setOpenMenu={props._setOpenMenu}
                removeEventListener={() => {
                }}
                addEventListener={() => {
                }}
                dispatchEvent={() => {
                    return false;
                }}
                onClick={() => {
                }}/> : ""}
        </header>
    )
}
Header.getInitialProps = ({query}) => {
    return {query}
};
