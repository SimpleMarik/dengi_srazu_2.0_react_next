import React, {useEffect} from "react";
import Input from "./pieces/Input";
import Button from "./pieces/Button";
import "../../scss/components/auth.scss";
import Link from "next/link";
import RegPages from "../layouts/RegPage";

export default function Login(props) {
    const authData = props.auth;
    let errors = authData.authErrors !== undefined ? authData.authErrors : "";
    // if (authData.authErrors !== undefined) {
    //     errors = authData.authErrors;
    // }

    useEffect(() => {
        document.addEventListener('keypress', (event) => {
            if(event.code === '13' && authData.authPhone >= 12 && authData.authPassword > 0) {
                console.log("press enter", event.code);
                props.submitAuth()
            }
        })
    }, [])

    return (
        <RegPages>
            <section className="auth">
                <div className="auth__wrapper">
                    <h1 className="auth__title">Вход в личный кабинет</h1>
                    <div className="auth__form-wrapper">
                        <div className="auth__form-block-wrapper">
                            <h2 className="auth__form-title">{authData.authSuccessMessage}</h2>
                            <h3 className="auth__form-title">Введите логин</h3>
                            <Input
                                mask="+7(999) 999 99 99"
                                type="text"
                                className="auth__form"
                                labelText="+7("
                                onFocus={() => props.clearError("phone")}
                                value={authData.authPhone}
                                onChange={(e) => props.setAuthPhone(e.target.value)}
                                error={errors.phone}
                            />
                            <Input
                                type="password"
                                className="auth__form"
                                labelText="введите пароль"
                                onFocus={() => props.clearError("password")}
                                value={authData.authPassword}
                                onChange={(e) => props.setAuthPassword(e.target.value)}
                                error={errors.password}
                            />
                            <Button
                                className="auth__btn-submit"
                                text="Войти"
                                onClick={() => props.submitAuth()}
                            />
                        </div>
                        <Link href={"/restore-password"}>
                            <a className="auth__btn-restore"> Забыли пароль?</a>
                        </Link>
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
