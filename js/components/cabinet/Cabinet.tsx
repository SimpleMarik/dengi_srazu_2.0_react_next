import React, {useEffect, useState} from "react";
import cookie from 'react-cookies';
import Router from 'next/router';
import RegPages from "../../layouts/RegPage";
import UserLoanContainer from "../../wrappedComponents/UserLoanContainer";
import Button from "../pieces/Button";
import "../../../scss/components/cabinet/cabinet.scss";
import UserServicesContainer from "../../wrappedComponents/UserServicesContainer";
import PrefferedLoanContainer from "../../wrappedComponents/PrefferedLoanContainer";
import {
    CABINET_CHANGE_PASSWORD_ICON,
    CABINET_FEEDBACK_ICON,
    CABINET_OUT_ICON,
    CABINET_USER_ICON,
    CREDIT_RAITING_ICON,
    LOAN_HISTORY_ICON,
    MY_LOAN_ICON,
    PREFFERED_LOAN_ICON,
    SERVICES_ICON
} from "../../constants/IconConstants";
import LoanHistoryContainer from "../../wrappedComponents/LoanHistoryContainer";
import FeedbackContainer from "../../wrappedComponents/FeedbackContainer";
import ProfileContainer from "../../wrappedComponents/ProfileContainer";
import ChangePasswordContainer from "../../wrappedComponents/ChangePasswordContainer";
import CreditRaitingContainer from "../../wrappedComponents/CreditRaitingContainer";
// import { submitAuth} from "../../store/auth/actions";
// import set = Reflect.set;

export default function Cabinet(props) {

    if (process.browser) {
        // const cook = cookie.load("Auth");
        // console.log(cook);
        // if (cookie.load("Auth") === undefined) {
        //     Router.push("/error", "/login");
        //
        // }
        useEffect(() => {
                // props.getOptions().then(() => {
                    props.isLogin();
                // })


        }, []);
    }
    const [state, setState] = useState({id: "myLoan"});
    state.id = "loanHistory";
    console.log(state.id);
    return (
        <RegPages>
            <section className="cabinet">
                <div className="cabinet__wrapper">
                    <div className="cabinet__menu">
                        <ul className="cabinet__menu-list">
                            {/*{[*/}
                            {/*    {text: "Мой займ", className: "cabinet__menu", onClick:onShowUserLoan(!showUserLoan)},*/}
                            {/*    {text: "Услуги", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "Предодобренный займ", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "История займов", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "Кредитный рейтинг", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "Обратная связь", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "Профиль", className: "cabinet__menu",onClick: " "},*/}
                            {/*].map(renderButton)}*/}

                            <li className={state.id === "myLoan" ? "cabinet__menu-item cabinet__menu-item--active" : "cabinet__menu-item"}>
                                <p className="cabinet__menu-item-img">
                                    {MY_LOAN_ICON}
                                </p>
                                <Button
                                    text="Мой займ"
                                    className="cabinet__menu-item-btn"
                                    onClick={() => {
                                        setState({id: "myLoan"})
                                        // onShowUserLoan(!showUserLoan)
                                    }}
                                />
                            </li>
                            {/*<li className={state.id === "services" ? "cabinet__menu-item cabinet__menu-item--active" : "cabinet__menu-item"}>*/}
                            {/*    <p className="cabinet__menu-item-img">*/}
                            {/*        {SERVICES_ICON}*/}
                            {/*    </p>*/}
                            {/*    <Button*/}
                            {/*        text="Услуги"*/}
                            {/*        className="cabinet__menu-item-btn"*/}
                            {/*        onClick={() => {*/}
                            {/*            setState({id: "services"})*/}
                            {/*        }}*/}
                            {/*    />*/}
                            {/*</li>*/}
                            {/*<li className={state.id === "prefferedLoan" ? "cabinet__menu-item cabinet__menu-item--active" : "cabinet__menu-item"}>*/}
                            {/*    <p className="cabinet__menu-item-img">*/}
                            {/*        {PREFFERED_LOAN_ICON}*/}
                            {/*    </p>*/}
                            {/*    <Button*/}
                            {/*        text="Предодобренный займ"*/}
                            {/*        className="cabinet__menu-item-btn"*/}
                            {/*        onClick={() => {*/}
                            {/*            setState({id: "prefferedLoan"})*/}
                            {/*        }}*/}
                            {/*    />*/}
                            {/*</li>*/}
                            <li className={state.id === "loanHistory" ? "cabinet__menu-item cabinet__menu-item--active" : "cabinet__menu-item"}>
                                <p className="cabinet__menu-item-img">
                                    {LOAN_HISTORY_ICON}
                                </p>
                                <Button
                                    text="История займов"
                                    className="cabinet__menu-item-btn"
                                    onClick={() => {
                                        setState({id: "loanHistory"})
                                    }}
                                />
                            </li>
                            {/*<li className={state.id === "creditRaiting" ? "cabinet__menu-item cabinet__menu-item--active" : "cabinet__menu-item"}>*/}
                            {/*    <p className="cabinet__menu-item-img">*/}
                            {/*        {CREDIT_RAITING_ICON}*/}
                            {/*    </p>*/}
                            {/*    <Button*/}
                            {/*        text="Кредитный рейтинг"*/}
                            {/*        className="cabinet__menu-item-btn"*/}
                            {/*        onClick={() => {*/}
                            {/*            setState({id: "creditRaiting"})*/}
                            {/*        }}*/}
                            {/*    />*/}
                            {/*</li>*/}
                            <li className={state.id === "cabinetFeedback" ? "cabinet__menu-item  cabinet__menu-item--active" : "cabinet__menu-item"}>
                                <p className="cabinet__menu-item-img">
                                    {CABINET_FEEDBACK_ICON}
                                </p>
                                <Button
                                    text="Обратная связь"
                                    className="cabinet__menu-item-btn"
                                    onClick={() => {
                                        setState({id: "cabinetFeedback"})
                                    }}
                                />
                            </li>
                            {/*<li className={state.id === "cabinetProfile" ? "cabinet__menu-item  cabinet__menu-item--active" : "cabinet__menu-item"}>*/}
                            {/*    <p className="cabinet__menu-item-img">*/}
                            {/*        {CABINET_USER_ICON}*/}
                            {/*    </p>*/}
                            {/*    <Button*/}
                            {/*        text="Профиль"*/}
                            {/*        className="cabinet__menu-item-btn"*/}
                            {/*        onClick={() => {*/}
                            {/*            setState({id: "cabinetProfile"})*/}
                            {/*        }}*/}
                            {/*    />*/}
                            {/*</li>*/}
                        </ul>
                        <ul className="cabinet__menu-list">
                            {/*{[*/}
                            {/*    {text: "Изменить пароль", className: "cabinet__menu",onClick: " "},*/}
                            {/*    {text: "Выход", className: "cabinet__menu",onClick: " "},*/}

                            {/*].map(renderButton)}*/}
                            <li className={state.id === "cabinetChangePassword" ? "cabinet__menu-item  cabinet__menu-item--active" : "cabinet__menu-item"}>
                                <p className="cabinet__menu-item-img">
                                    {CABINET_CHANGE_PASSWORD_ICON}
                                </p>
                                <Button
                                    text="Изменить пароль"
                                    className="cabinet__menu-item-btn"
                                    onClick={() => {
                                        setState({id: "cabinetChangePassword"})
                                    }}
                                />
                            </li>
                            <li className={"cabinet__menu-item"}>
                                <p className="cabinet__menu-item-img">
                                    {CABINET_OUT_ICON}
                                </p>
                                <Button
                                    text="Выход"
                                    className="cabinet__menu-item-btn"
                                    onClick={() => {
                                        // cookie.remove("Auth");
                                        // props.submitAuth();
                                        props.logoutUser();
                                        // Router.push("/");
                                    }}
                                />
                            </li>
                        </ul>

                    </div>
                    <div className="cabinet__view">
                        {state.id === "myLoan" ? <UserLoanContainer/> : ""}
                        {state.id === "services" ? <UserServicesContainer/> : ""}
                        {state.id === "prefferedLoan" ? <PrefferedLoanContainer/> : ""}
                        {state.id === "loanHistory" ? <LoanHistoryContainer/> : ""}
                        {state.id === "cabinetFeedback" ? <FeedbackContainer/> : ""}
                        {state.id === "creditRaiting" ? <CreditRaitingContainer/> : ""}
                        {state.id === "cabinetProfile" ? <ProfileContainer/> : ""}
                        {state.id === "cabinetChangePassword" ? <ChangePasswordContainer/> : ""}

                    </div>
                </div>
            </section>
        </RegPages>
    )
}

