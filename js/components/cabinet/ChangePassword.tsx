import React from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import "../../../scss/components/cabinet/change-password.scss";

export default function ChangePassword(props) {
    console.log(props);
    const data = props.cabinet.changePassword;
    const errors = data.changePasswordErrors !== undefined ? data.changePasswordErrors : "";
    const fieldErrors = errors.errors !== undefined ? errors.errors : "";
    console.log(errors.errors);
    return (
        <section className="change-password">
                <h2 className="change-password__title">Изменения пароля</h2>
                <p>{errors.message}</p>
                <div className="change-password__form">
                    <Input
                        type="text"
                        className="change-password__form"
                        labelText="Введите старый пароль"
                        onChange={(e) => props.setChangeOldPassword(e.target.value)}
                        value={data.oldChangePassword}
                        error={fieldErrors.newPassword}
                    />
                    <Input
                        type="text"
                        className="change-password__form"
                        labelText="Новый пароль"
                        onChange={(e) => props.setChangeNewPassword(e.target.value)}
                        value={data.newChangePassword}
                        error={fieldErrors.newPassword}
                    />
                    <Input
                        type="text"
                        className="change-password__form"
                        labelText="Новый пароль еще раз"
                        onChange={(e) => props.setChangeNewPasswordConfirm(e.target.value)}
                        value={data.newChangePasswordConfirm}
                        error={fieldErrors.confirmPassword}
                    />
                    <Button
                        className="change-password__btn-submit"
                        text="Отправить"
                        onClick={() => props.submitClientChangePassword()}
                    />
            </div>
        </section>
    )
}

