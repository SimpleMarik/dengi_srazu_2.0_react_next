import React, {ReactElement, ReactNode, useEffect, useState} from "react";
import moment from "moment";
// import cookie from 'react-cookies';
// import Router from 'next/router';
import "../../../scss/components/cabinet/history.scss"


export default function LoanHistory(props) {
    useEffect(() => {
        props.getAgreements();
    }, []);

    const formatDate = (date) => moment(date, 'DD-MM-YYYYY').locale('ru').format("DD.MM.YYYY");

    const formatStatus = (status) => {
        switch (status) {
            case -1:
                return "Просрочен"
            case 0:
                return "Закрыт"
            case 1:
                return "Активен"
        }
    }

    const [showSelectedAgreements, setShowSelectedAgreements] = useState("");
    const {agreements} = props.cabinet.agreements;

    const renderSelectedAgreements = () => {
        const selectedAgreements = props.base.hasOwnProperty('selectedAgreements') && props.base.selectedAgreements;
        return (
            <ul className="agreements__info-list">
                <li className="agreements__info-item">
                    <p className="agreements__info-data">
                        Сумма
                    </p>
                    <p className="agreements__info-data">
                        {selectedAgreements.body}
                    </p>
                </li>
                <li className="agreements__info-item">
                    <p className="agreements__info-data">
                        Дата возврата
                    </p>
                    <p className="agreements__info-data">
                        {formatDate(selectedAgreements.dateEnd)}
                    </p>
                </li>
                <li className="agreements__info-item">
                    <p className="agreements__info-data">
                        Статус
                    </p>
                    <p className="agreements__info-data">
                        {formatStatus(selectedAgreements.status)}
                    </p>
                </li>
            </ul>
        )
    }

    const renderAgreements = () => {
        const getInfoSelectedAgreements = (id) => {
            props.getSelectedAgreements(id);
        };

        let agreementArr: any[] = [];

        for (let index in agreements) {
            agreementArr.push(
                <li className={`agreements__item ${showSelectedAgreements === index && "agreements__item--selected"}`}
                    onClick={() => {
                        getInfoSelectedAgreements(agreements[index].id);
                        setShowSelectedAgreements(index)
                    }}>
                    Займ {agreements[index].number} от {formatDate(agreements[index].dateStart)}
                    {showSelectedAgreements &&
                    <>
                        {showSelectedAgreements === index && renderSelectedAgreements()}
                    </>}
                </li>
            )
        }
        return agreementArr;
    }

    return (
        <section className="history">
            <div className="history__wrapper">
                <h1 className="history__title">История займов</h1>
                <ul className="agreements__list">
                    {renderAgreements()}
                </ul>
            </div>
        </section>
    )
}

