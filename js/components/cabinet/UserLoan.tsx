import React, {useEffect} from "react";
import "../../../scss/components/cabinet/user-loan.scss";
import {WAIT_APPLY_ICON} from "../../constants/IconConstants";
import Input from "../pieces/Input";
import Select from "../pieces/Select";
import Button from "../pieces/Button";

export default function UserLoan(props) {
    useEffect(() => {
        // props.getUserLoan();
        // props.getClientAgreements();
    }, []);
    console.log(props.cabinet.userLoan.userLoanList);
    const updateForm = () => {
        return (
            <>
                <p className="user-loan__actual-title">Актуализация данных по заявке № 3284877</p>
                <p className="user-loan__text">Для продолжения рассмотрения заявки пожалуйста заполните
                    поля ниже и нажмите на копку &laquo;Сохранить&raquo;</p>
                <div className="user-loan__update-form">
                    <h2 className="user-loan__update-form-title">Проверьте правильность данных</h2>
                    <Input
                        type="text"
                        className="user-loan__update-form"
                        labelText="Инпут"
                    />
                    <Select
                        title="Выпадающий список"
                        className="user-loan__update-form"
                    />
                    <div className="user-loan__radio-wrapper">
                        <Input
                            type="radio"
                            className="user-loan__update-form"
                            checked="checked"
                            labelText="Активно"
                        />
                        <Input
                            type="radio"
                            className="user-loan__update-form"
                            labelText="Неактивно"
                        />
                    </div>
                    <div className="user-loan__doc-wrapper">
                        <Input
                            type="file"
                            className="user-loan__update-form"
                            labelText="Документы или фотография"
                        />
                        <Input
                            type="file"
                            className="user-loan__update-form"
                            labelText="Документы или фотография"
                        />
                        <Input
                            type="file"
                            className="user-loan__update-form"
                            labelText="Документы или фотография"
                        />
                    </div>
                    <Button
                        className="user-loan__btn-submit"
                        text="Сохранить"
                    />
                </div>
            </>
        )
    };

    const applicationData = () => {
        return (
            <>
                <div className="user-loan__sum-wrapper">
                    <p className="user-loan__sum">
                        12 500 ₽
                    </p>
                    <p className="user-loan__sum-text">
                        Сумма займа
                    </p>
                </div>
                <p className="user-loan__application">
                    Ваша заявка на рассмотрении №2981068
                </p>
                <p className="user-loan__return-sum">
                    Сумма возврата 6 725 ₽
                </p>
                <p className="user-loan__text">
                    Все заявки рассматриваются в порящдке очереди.
                </p>
                <p className="user-loan__text">
                    О решении по заявке Вас проинформируют в виде смс - сообщения,
                    будет звонить специалист службы безопасности.
                </p>

            </>
        )
    };
    const show = false;
    return (
        <section className="user-loan">
            <div className="user-loan__wrapper">
                <p className="user-loan__img-wrapper">
                    {WAIT_APPLY_ICON}
                </p>
                {show ? updateForm() : applicationData()}
            </div>
        </section>
    )
}

