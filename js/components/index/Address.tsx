import React from "react";
import Button from '../pieces/Button';
// import Input from "../pieces/Input.jsx";
// import {withRouter} from 'react-router-dom';

import {
    REGION_ICON,
    PHONE_ADDRESS_ICON,
    GPS_ICON_1, GPS_ICON_2, GPS_ICON_3
} from "../../constants/IconConstants";

export default function Address() {

    return (
        <React.Fragment>
            <section className="address">

                <div className="address__wrapper">

                    <h2 className={'address__title'}>Адреса офисов</h2>

                    <div className="address__link-wrapper">
                        <a className={"address__link"} href="#">
                            <p>
                                {REGION_ICON}
                            </p>
                            <p className={"address__link-text"}>
                                Выбрать регион
                            </p>
                        </a>

                        <a className={"address__link"} href="#">
                            <p>
                                {PHONE_ADDRESS_ICON}
                            </p>
                            <p className={"address__link-text"}>
                                8 800 700 74 24
                            </p>
                        </a>
                    </div>

                    <Button
                        type="button"
                        className="address__btn-call"
                        text="Заказать звонок"
                    />

                    <Button
                        type="button"
                        className="address__btn-office"
                        text="Показать офисы"
                    />

                    <p className={'address__gps-one'}>{GPS_ICON_1}</p>
                    <p className={'address__gps-two'}>{GPS_ICON_2}</p>
                    <p className={'address__gps-three'}>{GPS_ICON_3}</p>

                </div>

            </section>
        </React.Fragment>
    )
}

