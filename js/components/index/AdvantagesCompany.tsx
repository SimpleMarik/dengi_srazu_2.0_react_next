import React from "react";

export default function AdvantagesCompany() {
    return (
        <React.Fragment>
            <section className="advantages-company">
                <h2 className="advantages-company__title">
                    О компании
                </h2>
                <div className="advantages-company__wrapper">
                    <ul className="advantages-company__list">
                        <li className="advantages-company__item">
                            <h3 className="advantages-company__item-title">
                                Быстро, удобно и безопасно
                            </h3>
                            <p className="advantages-company__item-content">
                                У каждого из нас бывают ситуации, когда появляются незапланированные расходы: срочные
                                поездки, подарок
                                близкому человеку, поломка любимого телефона, задержка заработной платы. В ритме нашей
                                жизни
                                мы не всегда
                                можем позволить себе ждать решения вопроса или занимать деньги у близких. Займ небольшой
                                суммы на короткий срок
                                сможет удовлетворить множество внезапно появившихся нужд.
                            </p>
                        </li>
                        <li className={"advantages-company__item advantages-company__item-content-comment"}>
                            <p className="advantages-company__item-content-comment-text">
                                «Неплохой сервис, быстрое одобрение, лояльный подход к любым ситуациям, оповещение о
                                оплате за много дней до оплаты.Хорошее мфо, будем рады воспользоваться еще!»
                            </p>

                            <p className={"advantages-company__item-content-comment-name"}>Виктория Романова</p>
                        </li>
                        <li className="advantages-company__item">
                            <h3 className="advantages-company__item-title">
                                Срочно понадобились деньги?
                            </h3>
                            <h4 className="advantages-company__item-block-title">
                                Процесс займет не более 15 минут
                            </h4>
                            <p className="advantages-company__item-content">
                                Займ небольшой суммы на короткий срок сможет удовлетворить множество внезапно
                                появившихся
                                нужд.
                                Пользоваться услугами «Деньги сразу» легко и просто: микрозаймы выдаются на срок 16 дней
                                в
                                сумме от
                                1 000 до 30 000 рублей. Для получения займа необходимы лишь паспорт гражданина РФ и 20
                                минут
                                свободного времени.
                            </p>

                            <p className="advantages-company__item-content">
                                Микрозаймы выдаются на срок 16 дней в сумме от 1 000 до 30 000 рублей. Для получения
                                займа необходимы лишь паспорт гражданина РФ и 20 минут свободного времени.
                            </p>

                            <p className="advantages-company__item-content">
                                Вы получите предварительное решение в смс-сообщении для понимания, будут ли в дальнейшем
                                рассмотрены Ваши данные в офисе в целях предоставления займа. Вы можете «Заказать
                                звонок» и наш Специалист свяжется с Вами для оформления, также Вы можете оформить заявку
                                на сайте самостоятельно.
                            </p>

                            <p className="advantages-company__item-content">
                                Вы можете продлить срок пользования суммой займа, для этого Вам необходимо обратиться в
                                офис Компании и произвести оплату процентов за прошедший срок. Произвести оплату
                                процентов нужно не позднее 14-го дня после дня окончания договора.
                            </p>
                        </li>
                        <li className="advantages-company__item">
                            <h3 className="advantages-company__item-title">
                                Кто может получить займ?
                            </h3>
                            <p className="advantages-company__item-content">
                                Займ небольшой суммы на короткий срок сможет удовлетворить множество внезапно
                                появившихся
                                нужд. Пользоваться услугами «Деньги сразу» могут:
                            </p>
                            <ul className="advantages-company__block-list">
                                <li className="advantages-company__block-item">Физические лица с испорченной кредитной
                                    историей длинная строчка в две строки;
                                </li>
                                <li className="advantages-company__block-item">Люди пенсионного и предпенсионного
                                    возраста;
                                </li>
                                <li className="advantages-company__block-item">Заемщики с неподтвержденным доходом;</li>
                                <li className="advantages-company__block-item">Учащиеся ВУЗов и колледжей.</li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </section>
        </React.Fragment>
    )
};

