import React, {useEffect, useState} from "react";
import Input from "../pieces/Input";
import Slider from 'react-rangeslider';
import EsiaBtn from "./EsiaBtn";
import Link from "next/link";

export default function Calc(props) {
    if (process.browser) {
        useEffect(() => {
            // props._getDefaultData();
            props.setCalcSum();
        }, []);
    }

    const data = props;
    const {base} = props;
    const baseCalc = base.baseCalc !== undefined ? base.baseCalc : "";
    const defaultCalc = base.defaultCalc !== undefined ? base.defaultCalc : "";
    const {selectedSum, returnSum} = base;
    const percent = baseCalc ? baseCalc.percent : defaultCalc.percent;
    const {currentDay} = baseCalc ? baseCalc : defaultCalc;
    const minSum = parseInt(baseCalc.minSum) ? parseInt(baseCalc.minSum) : defaultCalc.minSum;
    const maxSum = parseInt(baseCalc.maxSum) ? parseInt(baseCalc.maxSum) : defaultCalc.maxSum;

    return (
        <React.Fragment>
            <section className="calc">

                <div className={"calc__wrapper"}>
                    <h1 className="calc__title">
                        Займ до 30000 ₽ на вашу карту
                    </h1>

                    <div className="calc__range-slider-wrapper">
                        <p className="calc__range-summ">
                            {/*{baseCalc.stepSum ? baseCalc.stepSum + " " : "1000"}*/}
                            {selectedSum ? selectedSum : defaultCalc.currentSum}
                            ₽</p>
                        <div className="calc__range-wrapper rangeslider">
                            <Slider
                                value={selectedSum ? selectedSum : 6000}
                                min={1000}
                                max={27000}
                                step={1000}
                                orientation="horizontal"
                                onChange={props.setCalcSum}
                                tooltip={false}
                            />
                        </div>
                        <p className="calc__range-summ">
                            {16}
                            дней</p>
                        <div className="calc__range-wrapper rangeslider">
                            <Slider
                                value={props.setDay ? props.setDay : 16}
                                min={props.dayMin}
                                max={props.dayMax}
                                step={props.dayStep}
                                orientation="horizontal"
                                onChange={props.setDay}
                                tooltip={false}
                            />
                        </div>

                        <ul className="calc__output">
                            <li className="calc__output-item">
                                <p className="calc__output-description">
                                    Дата возврата
                                </p>
                                <p className="calc__output-value">
                                    {base.returnDate}
                                </p>
                            </li>
                            <li className="calc__output-item">
                                <p className="calc__output-description">
                                    Сумма возврата
                                </p>
                                <p className="calc__output-value">
                                    {returnSum} ₽
                                </p>
                            </li>
                            {/*//пока что отложено*/}
                            {/*<li className={"calc__output-item"}>*/}
                            {/*    <p className={"calc__output-description"}>*/}
                            {/*        Платеж раз в 2 недели*/}
                            {/*    </p>*/}
                            {/*    <p className={"calc__output-value"}>*/}
                            {/*        {}*/}
                            {/*    </p>*/}
                            {/*</li>*/}
                        </ul>
                        <Link href="/cabinet"><a className="calc__take-loan-btn">Получить займ</a></Link>

                    </div>

                    {/*<EsiaBtn/>*/}
                </div>
                {/*<p className="calc__footnote">Выдача займов до 120 000 рублей</p>*/}
            </section>
        </React.Fragment>
    )

}
