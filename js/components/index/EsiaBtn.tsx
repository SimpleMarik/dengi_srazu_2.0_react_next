import React,{ Component } from "react";
import {COLOR_ESIA_BTN} from "../../constants/IconConstants";

export default class EsiaBtn extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="esia-btn__wrapper">
                    <a className="esia-btn__link" href="https://esia.gosuslugi.ru/aas/oauth2/ac?client_id=OCM&amp;client_secret=MIIFHgYJKoZIhv
                cNAQcCoIIFDzCCBQsCAQExDzANBglghkgBZQMEAgEFADALBgkqhkiG9w0BBwGgggKrMIICpzCCAhACCQDDPSX1tgMrXjANBg
                kqhkiG9w0BAQsFADCBlzELMAkGA1UEBhMCUlUxFjAUBgNVBAgMDVJvc3RvdiByZWdpb24xFjAUBgNVBAcMDVJvc3Rvdi1vbi
                1Eb24xFjAUBgNVBAoMDU9uZWNsaWNrbW9uZXkxDDAKBgNVBAsMA09DTTEMMAoGA1UEAwwDT0NNMSQwIgYJKoZIhvcNAQkBFhVs
                b2FuQG9uZWNsaWNrbW9uZXkucnUwHhcNMTcxMTEzMTE1OTAwWhcNMjcxMTExMTE1OTAwWjCBlzELMAkGA1UEBhMCUlUxFjAUBg
                NVBAgMDVJvc3RvdiByZWdpb24xFjAUBgNVBAcMDVJvc3Rvdi1vbi1Eb24xFjAUBgNVBAoMDU9uZWNsaWNrbW9uZXkxDDAKBgNVB
                AsMA09DTTEMMAoGA1UEAwwDT0NNMSQwIgYJKoZIhvcNAQkBFhVsb2FuQG9uZWNsaWNrbW9uZXkucnUwgZ8wDQYJKoZIhvcNAQEBBQA
                DgY0AMIGJAoGBAN-NuqaOtR_01N8q4zfQEnQ2sL8SZyFRgSewsPhOErBwnJiLWpJYpX_9HZycRWMNowQ4_ARssxeG7-sN-dpaL4q
                c0wZPF0nRCx3gc7s3BN5_whCFTNHjKIa19BxOjFx7zb6CWHHOEYxMkINFd2Ysfrg3v6zKI4O3K4vSp2q3b7IRAgMBAAEwDQYJK
                oZIhvcNAQELBQADgYEAWtlr_FD4araIR7cOAbLHARJlWlRsvG0WMoGhXsQiWiscFJhp0fEL78lIny59-8Ij4Xh2mZXqYD1T3l
                Xu9Qd3gZlLXgdRQUiLpghiD7COGaXWyc8XmEPud4oTfImus0hwPRlg_MRojR1XfyDDMd1SSJeLKPnYNp_UfL6xwWR3G48xggI3MI
                ICMwIBATCBpTCBlzELMAkGA1UEBhMCUlUxFjAUBgNVBAgMDVJvc3RvdiByZWdpb24xFjAUBgNVBAcMDVJvc3Rvdi1vbi1Eb24xFj
                AUBgNVBAoMDU9uZWNsaWNrbW9uZXkxDDAKBgNVBAsMA09DTTEMMAoGA1UEAwwDT0NNMSQwIgYJKoZIhvcNAQkBFhVsb2FuQG9uZ
                WNsaWNrbW9uZXkucnUCCQDDPSX1tgMrXjANBglghkgBZQMEAgEFAKCB5DAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSq
                GSIb3DQEJBTEPFw0xOTA3MDIwOTM5MDFaMC8GCSqGSIb3DQEJBDEiBCB0juQSw21NvQUf2axXHP9cthbN_vJz9rJCWav_zS2N3D
                B5BgkqhkiG9w0BCQ8xbDBqMAsGCWCGSAFlAwQBKjALBglghkgBZQMEARYwCwYJYIZIAWUDBAECMAoGCCqGSIb3DQMHMA4GCCqG
                SIb3DQMCAgIAgDANBggqhkiG9w0DAgIBQDAHBgUrDgMCBzANBggqhkiG9w0DAgIBKDANBgkqhkiG9w0BAQEFAASBgIh8YqCtL28r
                CRTElbLMBnyRbu4xrQyQ-RyPgJJ_InhavBQoEp7Y9m7ixCkOWjD_wCnTLtzMdfiqk35I_clhozsa6r_jHznBfdsYPOMcGOpObLdC
                _OgPXhckBQdNRuGwrUoQ97LhtlU3uN45AN34ePIRnM0GQSzQoQ3YRAJS2qCN&amp;redirect_uri=https%3A%2F%2Foneclickm
                oney.ru%2Flogin%2Fesia&amp;scope=fullname+id_doc&amp;response_type=code&amp;state=09ff6aca-49ef-4f99-8
                56d-2415e4ca5ea0&amp;access_type=offline&amp;timestamp=2019.07.02+12%3A39%3A01+%2B0300">
                        Войти через госуслуги
                    </a>
                    {COLOR_ESIA_BTN}
                </div>
            </React.Fragment>
        )
    }
}
