import * as React from "react";
import PaySystem from "../../components/index/PaySystem";
import AdvantagesCompany from "./AdvantagesCompany";
import CalcContainer from "../../wrappedComponents/CalcContainer";
import "../../../scss/main.scss";
import Pages from '../../layouts/Pages'
import MoreProducts from "./MoreProducts";
import Reviews from "./Reviews";
import Mobile from "./Mobile";
import PayWithoutEnteringContainer from "../../wrappedComponents/PayWithoutEnteringContainer";
import Address from "./Address";


export default function Index() {
    return (
        <Pages>
            <main className="main">
                <CalcContainer/>
                {/*<PaySystem/>*/}
                <PayWithoutEnteringContainer/>
                <Address/>
                <Mobile/>
                {/*<Reviews/>*/}
                {/*<MoreProducts/>*/}

                <AdvantagesCompany/>
            </main>
        </Pages>
    )
}

