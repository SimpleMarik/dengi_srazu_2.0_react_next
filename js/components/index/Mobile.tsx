import React from "react";
import Button from '../pieces/Button';
// import Input from "../pieces/Input.jsx";
// import {withRouter} from 'react-router-dom';

import {
    ANDROID_ICON,
    APP_STORE_ICON,
    // CARD_TRANSFER_ICON,
    // CREDIT_REITING_ICON,
    GOOGLE_PLAY_ICON, IOS_ICON,
    // LOAN_PTS_ICON
} from "../../constants/IconConstants";
import Input from "../pieces/Input";

export default function Mobile() {

    return (
        <React.Fragment>
            <section className="mobile">

                <div className="mobile__wrapper">

                    <div className="mobile__left">
                        <h2 className={'mobile__title'}>Скачай мобильное приложение</h2>

                        <p className={'mobile__text'}>И оформи заявку на получение суммы до 30000 ₽ на срок от 6 до 16 дней всего за 15 минут</p>

                        <div className="mobile__icons-wrapper">
                            <p className="mobile__icons-item">
                                {GOOGLE_PLAY_ICON}
                            </p>
                            <p className="mobile__icons-item">
                                {APP_STORE_ICON}
                            </p>
                        </div>

                        <p className={'mobile__text'}>Или оставьте номер и мы вышлем СМС-сообщение со ссылкой на приложение</p>

                        <div className="mobile__form-wrapper">

                            <Input
                                id="phone"
                                type="text"
                                className="mobile__form"
                                placeholder="+7"
                            />

                            <Button
                                type="button"
                                className="mobile__form-btn"
                                text="Отправить ссылку"
                            />

                        </div>
                    </div>

                    <div className={"mobile__right"}>
                        <img className={"mobile__img"} src="../../../static/images/mobile-tag.png" alt=""/>
                    </div>

                </div>

            </section>
        </React.Fragment>
    )
}

