import React from "react";
// import Button from '../pieces/Button.jsx';
// import Input from "../pieces/Input.jsx";

// import Slider from "react-slick";
import {CARD_TRANSFER_ICON, CREDIT_REITING_ICON, LOAN_PTS_ICON} from "../../constants/IconConstants";


export default function MoreProducts() {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         countSlides: 0,
    //         opacity: "0",
    //     }
    // }
    //
    // render() {
    // const settings = {
    //     dots: true,
    //     infinite: true,
    //     speed: 500,
    //     slidesToShow: 1,
    //     slidesToScroll: 1
    // };

    return (
        <React.Fragment>
            <section className="more-products">
                <h2 className="more-products__title">
                    Другие продукты
                </h2>
                <ul className="more-products__list">
                    <li className="more-products__item">
                        <p className="more-products__item-wrapper">
                            {CREDIT_REITING_ICON}
                        </p>
                        <p className="more-products__item-text">
                            Кредитный рейтинг
                        </p>
                    </li>
                    <li className="more-products__item">
                        <p className="more-products__item-wrapper">
                            {LOAN_PTS_ICON}
                        </p>
                        <p className="more-products__item-text">
                            Получение займа под ПТС
                        </p>
                    </li>
                    <li className="more-products__item">
                        <p className="more-products__item-wrapper">
                            {CARD_TRANSFER_ICON}
                        </p>
                        <p className="more-products__item-text">
                            Переводы с карты
                        </p>
                    </li>
                </ul>
            </section>
        </React.Fragment>
    )
}
