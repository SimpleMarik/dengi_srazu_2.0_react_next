import React from "react";
import {
    PAY_MAESTRO_ICON,
    PAY_MASTERCARD_ICON,
    PAY_MIR_ICON,
    PAY_VISA_ICON,
    PAY_YANDEX_ICON
} from "../../constants/IconConstants";

export default function PaySystem() {
    return (
        <div className="pay-system">
            <h2 className="pay-system__title">Удобные для вас платежные системы</h2>
            <ul className={"pay-system__list"}>
                <li className={"pay-system__item"}>
                    <p className={"pay-system__image-wrapper"}>
                        {PAY_MIR_ICON}
                    </p>
                </li>
                <li className={"pay-system__item"}>
                    <p className={"pay-system__image-wrapper"}>
                        {PAY_MASTERCARD_ICON}
                    </p>
                </li>
                <li className={"pay-system__item"}>
                    <p className={"pay-system__image-wrapper"}>
                        {PAY_VISA_ICON}
                    </p>
                </li>
                <li className={"pay-system__item"}>
                    <p className={"pay-system__image-wrapper"}>
                        {PAY_MAESTRO_ICON}
                    </p>
                </li>
                <li className={"pay-system__item"}>
                    <p className={"pay-system__image-wrapper"}>
                        {PAY_YANDEX_ICON}
                    </p>
                </li>
            </ul>
        </div>
    )
}
