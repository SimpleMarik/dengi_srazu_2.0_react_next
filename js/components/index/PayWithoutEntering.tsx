import React, {useState} from "react";
import Button from '../pieces/Button';
import Input from "../pieces/Input";

export default function PayWithoutEntering() {

    const [input, setInput] = useState('phone');

    return (
        <React.Fragment>
            <section className="pay-without-entering">

                <div className="pay-without-entering__form">
                    <h2 className="pay-without-entering__form-title">
                        Оплатите займ без авторизации
                    </h2>

                    <p className={"pay-without-entering__form-text"}>Найти займ по номеру</p>

                    <div className="pay-without-entering__form-choose">
                        <Button
                            type="button"
                            className={(input == 'phone' ? 'active ' : '') + "pay-without-entering__form-choose-btn"}
                            text="Телефон"
                            onClick={function () {
                                setInput('phone')
                            }}
                        />
                        <Button
                            type="button"
                            className={(input == 'phone' ? '' : 'active ') + "pay-without-entering__form-choose-btn"}
                            text="Номер договора"
                            onClick={function () {
                                setInput('number')
                            }}
                        />
                    </div>

                    <div className="pay-without-entering__form-inputs">
                        {input == 'phone' ? <Input
                            id="contractNumber"
                            type="text"
                            className="pay-without-entering__form"
                            placeholder="+7"
                        /> : <Input
                            id="contractNumber"
                            type="text"
                            className="pay-without-entering__form"
                            placeholder="Введите № договора"
                        />}

                        <Button
                            type="button"
                            className="pay-without-entering__form-btn-pay"
                            text="Найти"
                        />
                    </div>
                </div>

            </section>
        </React.Fragment>
    )
}

