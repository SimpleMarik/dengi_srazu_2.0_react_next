import React from "react";
import Slider from "react-slick";
import {slidersSettings} from "../../helpers/helpers";

export default function Reviews() {
    const settings = slidersSettings(1,3);
    return (
        <React.Fragment>
            <section className="review">
                <div className="review__slider">
                    <h2 className="review__slider-title">
                        О нас говорят
                    </h2>
                    <Slider {...settings}>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>
                                ИРИНА
                            </p>
                            <p className="review__slider-text">Отличный сервис, выручили, большое спасиба. Советую
                                обращаться. Отличная компания, пользуюсь не первый раз, все быстро и прозрачно,
                                деньги поступают моментально. Спасибо Деньги Сразу! Всем советую!</p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Иван</p>
                            <p className="review__slider-text">Отличная компания, пользуюсь не первый раз, все быстро
                                и прозрачно, деньги поступают моментально. Спасибо Деньги Сразу! Всем советую!</p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Юлия</p>
                            <p className="review__slider-text">Благодарю Компанию за доверие,
                                быстроту оформления заявки!</p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Виталий Иващенко</p>
                            <p className="review__slider-text">Отличный сервис, выручили, большое спасибо.
                                Советую обращаться.</p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Анна</p>
                            <p className="review__slider-text">Хороший сервис. Быстро и без проблем получила деньги!!!
                            </p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Карина Лазукова</p>
                            <p className="review__slider-text">Всё очень быстро и удобно, советую обращаться. Спасибо!</p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>Олеся Дубовицких</p>
                            <p className="review__slider-text">Быстро одобряют. Беру уже 2 раза. Все хорошо.
                                Проценты приемлемые. Рекомендую.</p>
                        </div>
                    </Slider>
                </div>
            </section>
        </React.Fragment>
    )
}

