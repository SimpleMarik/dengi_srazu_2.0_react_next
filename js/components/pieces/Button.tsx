import React from "react";

export default function Button(props) {
    return (
        <div className={props.className + "-wrapper"}>
            <button
                id={props.id}
                type="button"
                className={props.className}
                onClick={props.onClick}
            >
                {props.text}
            </button>
            <p className={props.switch ? props.hintClass : "hide"}>
                {props.hintText}
            </p>
        </div>
    )
}


