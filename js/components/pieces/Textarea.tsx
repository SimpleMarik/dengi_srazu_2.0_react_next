import React from "react";

export default function Input(props) {
    const renderError = () => {
        return (
            <>
                {error ? error.map((i) => {
                    return <p className={props.className + "-texarea-error"} key={i}>{i}</p>
                }) : ""}
            </>
        )
    };
    let error = props.error;

    return (
        <>
            <div className={props.value ? props.className + "-textarea-wrapper--full" + " " + props.className + "-textarea-wrapper-wrapper" : props.className + "-textarea-wrapper"}>
                <div className={props.className + "-textarea"}>
                    <textarea
                        id={props.id}
                        className={props.className + "-textarea-input"}
                        onChange={props.onChange}
                        onClick={props.onClick}
                        onFocus={props.onFocus}
                        value={props.value}
                        defaultValue={props.defaultValue}
                        ref={props.ref}
                        placeholder={props.placeholder}
                        name={props.name}
                        disabled={props.disabled}
                        wrap={props.wrap}
                        cols={props.cols}
                        rows={props.rows}
                    >
                    </textarea>
                </div>
                {error ? renderError() : ""}
            </div>
        </>
    )
}
