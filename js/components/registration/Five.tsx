import React, {useEffect} from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import "../../../scss/components/registration/step5.scss";
import RegPages from "../../layouts/RegPage";


export default function Five(props) {
    useEffect(() => {
        document.addEventListener('keydown', function (e) {
            if (e.keyCode === 113) {
                props.runTestStep5();
            }
        });
    });

    const renderPayCards = () => {

        return (
            <ul className="registration-step5__card-list">
                <li className="registration-step5__card-item">
                    <Input
                        type="radio"
                        id="selectCard"
                        name="selectCard"
                        className="registration-step5__form"
                        channel-id="1"
                        checked={goTest ? true : regData.selectedChannel}
                        onClick={props.setMoneyChannelId}

                    />
                    <div className="registration-step5__card-wrapper">
                        <p className={regData.selectedChannel ? "registration-step5__card-title" :
                            "registration-step5__card-title registration-step5__card-title--disabled"}>
                            Перевод на платежную карту Visa / MasterCard / Maestro / МИР
                        </p>
                        <p className={regData.selectedChannel ? "registration-step5__card-title" :
                            "registration-step5__card-title registration-step5__card-title--disabled"}>
                            Мгновенное зачисление на большинство банковских карт
                        </p>
                        <Input
                            type="text"
                            labelText="Введите номер карты"
                            className="registration-step5__form"
                            disabled={goTest ? false : !regData.selectedChannel}
                            value={goTest ? testData.card.cardNumber : regData.card.cardNumber}
                            onChange={(e) => {
                                props.setCardNumber(e.target.value);
                            }}
                        />
                        <div className="registration-step5__form-date-wrapper">
                            <Input
                                mask="99"
                                type="text"
                                labelText="ММ"
                                className="registration-step5__form"
                                disabled={goTest ? false : !regData.selectedChannel}
                                value={goTest ? testData.card.cardYear : regData.card.cardMonth}
                                onChange={(e) => props.setCardMonth(e.target.value)}
                            />
                            <span className={!regData.selectedChannel ? "separator-disabled" : ""}>/</span>
                            <Input
                                mask="99"
                                type="text"
                                labelText="ГГ"
                                className="registration-step5__form"
                                disabled={goTest ? false : !regData.selectedChannel}
                                value={goTest ? testData.card.cardMonth : regData.card.cardYear}
                                onChange={(e) => props.setcardsYear(e.target.value)}
                            />
                        </div>
                        <p className="registration-step5__form-info">Срок зачисления </p>
                        <p className="registration-step5__form-info">От 1 часа до трёх рабочих дней</p>
                    </div>
                </li>

            </ul>
        )
    };
    const regData = props.registration.regStep5Reducer;
    const testData = regData.testData;
    const goTest = regData.goTest;
    return (
        <RegPages>
            <section className="registration-step5">
                <h1 className="registration-step5__title">Выбор способа получения</h1>
                <div className="registration-step5__wrapper">
                    <div className="registration-step4__form">
                        {renderPayCards()}
                        <Button
                            text="Далее"
                            className="registration-step5__btn-submit"
                            onClick={() => props.submitStepFive()}
                        />
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
