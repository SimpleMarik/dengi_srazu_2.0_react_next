import React, {useEffect} from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import "../../../scss/components/registration/step4.scss";

export default function Four(props) {

    useEffect(() => {
        if (code !== null && code !== undefined) {
            props.sentActivateCode()
        }
    }, []);
    const regData = props.registration.regStep4Reducer;
    const code = regData.smsCode;
    const errors = regData.step4Errors;
    const errorMessage = regData.step4Message;
    return (
        <section className="registration-step4">
            <div className="registration-step4__wrapper">
                <h1 className="registration-step4__title">Предварительная заявка</h1>
                <p>{code} ваш код</p>
                <div className="registration-step4__form">
                    <h2 className="registration-step4__form-title">
                        Активация заявки по смс
                    </h2>
                    <Input
                        mask="999999"
                        type="text"
                        id="activateCode"
                        labelText="Введите код активации"
                        className="registration-step4__form"
                        value={regData.activateCode}
                        onChange={(e) => props.setActivateCode(e.target.value)}
                        error={errors.activateCode ? ["Код активации обязателен для заполнения"] : errorMessage[0] ? ["Код активации введен неправильно"] : ""}
                        onFocus={() => props.clearErrorFour("activateCode")}
                    />
                    <Button
                        text="Далее"
                        type="button"
                        className="registration-step4__btn-submit"
                        onClick={() => props.submitStepFour(props.history)}
                    />
                </div>
            </div>
        </section>
    )
}
