import React, {useEffect} from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import Select from "../pieces/Select";
import "../../../scss/components/registration/step3.scss";

export default function Step3(props) {
    useEffect(() => {
        props.getDefaultDataStep3();
        document.addEventListener('keydown', function (e) {
            if (e.keyCode === 113) {
                props.runTestStep3();
            }
        });
    }, []);


    const showOpenedCredit = () => {
        return (
            <Input
                type="text"
                id="amountPayMonthlyCredits"
                className="registration-step3__form"
                labelText="Сумма платежей по открытым кредитам"
                value={goTest ? testData.amountPayMonthlyCredits : regData.amountPayMonthlyCredits}
                onChange={(e) => props.setAmountPayMonthlyCredits(e.target.value)}
                onFocus={() => props.clearError("amountPayMonthlyCredits")}
                error={errors.amountPayMonthlyCredits}

            />
        )
    };

    const showOpenedLoans = () => {
        return (
            <Input
                type="text"
                id="amountPayMonthlyLoans"
                className="registration-step3__form"
                labelText="Сумма платежей по открытым займам"
                value={goTest ? testData.amountPayMonthlyLoans : regData.amountPayMonthlyLoans}
                onChange={(e) => props.setAmountPayMonthlyLoans(e.target.value)}
                onFocus={() => props.clearError("amountPayMonthlyLoans")}
                error={errors.amountPayMonthlyLoans}
            />
        )
    };

    const renderWorkInfo = () => {
        return (
            <div>
                <Input
                    type="text"
                    id="workName"
                    className="registration-step3__form"
                    labelText="Место работы"
                    value={goTest ? testData.workName : regData.workName}
                    onChange={(e) => props.setWorkName(e.target.value)}
                    onFocus={() => props.clearError("workName")}
                    error={errors.workName}
                />
                <Input
                    type="text"
                    id="positionName"
                    className="registration-step3__form"
                    labelText="Должность"
                    value={goTest ? testData.positionName : regData.positionName}
                    onChange={(e) => props.setPositionName(e.target.value)}
                    onFocus={() => props.clearError("positionName")}
                    error={errors.positionName}

                />
                <Input
                    type="text"
                    id="workAddress"
                    className="registration-step3__form"
                    labelText="Адрес места работы"
                    value={goTest ? testData.workAddress : regData.workAddress}
                    onChange={(e) => props.setWorkAddress(e.target.value)}
                    onFocus={() => props.clearError("workAddress")}
                    error={errors.workAddress}

                />
                <Input
                    type="text"
                    id="phoneWork"
                    className="registration-step3__form"
                    labelText="Рабочий номер телефона"
                    value={goTest ? testData.phoneWork : regData.phoneWork}
                    onChange={(e) => props.setPhoneWorks(e.target.value)}
                    onFocus={() => props.clearError("phoneWork")}
                    error={errors.phoneWork}
                />
                <Select
                    title={goTest ? "От 3 до 6 месяцев" : "Стаж работы на последнем месте"}
                    data={regData.lastWorkLength}
                    dataAtr={"ext_id"}
                    onClick={props.setSelectedLastWorkLength}
                    className="registration-step3__form"
                    onFocus={() => props.clearError("workLengthLastTypeId")}
                    error={errors.workLengthLastTypeId}
                />
            </div>
        )
    };

    const regData = props.registration.regStep3Reducer;
    const guarantorIncome = regData.guarantorIncome;
    const guarantorResidence = regData.guarantorResidence;
    const maritalStatus = regData.maritalStatus;
    const loanPurposes = regData.loanPurpose;
    const education = regData.education;
    const goTest = regData.goTest;
    const testData = regData.testData;
    const errors = regData.step3Errors;
    // console.log(errors);

    return (
        <section className="registration-step3">
            <div className="registration-step3__wrapper">
                <h1 className="registration-step3__title">Оплата займа</h1>
                <div className="registration-step3__form">
                    <h2 className="registration-step3__form-title"> {/*исправить на 3*/}
                        Заполните данные о работе
                    </h2>
                    <div>
                        <h2 className="registration-step3__form-block-title">Контактное лицо №1</h2>
                        <Select
                            title={goTest ? testData.guarantorIncome.extId : "Тип гаранта"}
                            data={guarantorIncome}
                            dataAtr={"ext_id"}
                            onClick={props.setSelectedGuarantorIncome}
                            className="registration-step3__form"
                            onFocus={() => props.clearError("guarantorIncome.extId")}
                            error={errors["guarantorIncome.extId"]}
                        />
                        <Input
                            type="text"
                            id="firstGuarantName"
                            className="registration-step3__form"
                            labelText="ФИО гаранта"
                            value={goTest ? testData.guarantorIncome.FIO : regData.firstGuarantName}
                            onChange={(e) => props.setFirstGuarantName(e.target.value)}
                            onFocus={() => props.clearError("guarantorIncome.FIO")}
                            error={errors["guarantorIncome.FIO"]}
                        />
                        <Input
                            mask="+7(999) 999 99 99"
                            type="text"
                            id="firstGuarantTelephone"
                            className="registration-step3__form"
                            labelText="+7 ("
                            value={goTest ? testData.guarantorIncome.phone : regData.firstGuarantTelephone}
                            onChange={(e) => props.setFirstGuarantTelephone(e.target.value)}
                            onFocus={() => props.clearError("guarantorIncome.phone")}
                            error={errors["guarantorIncome.phone"]}
                        />
                    </div>
                    <div>
                        <h2 className="registration-step3__form-block-title">Контактное лицо №2 </h2>
                        <Select
                            title={goTest ? testData.guarantorResidence.FIO : "Тип гаранта"}
                            data={guarantorResidence}
                            dataAtr={"ext_id"}
                            onClick={props.setSelectedGuarantorResidence}
                            className="registration-step3__form"
                            onFocus={() => props.clearError("guarantorResidence.extId")}
                            error={errors["guarantorResidence.extId"]}
                        />
                        <Input
                            type="text"
                            id="secondGuarantName"
                            className="registration-step3__form"
                            labelText="ФИО гаранта"
                            value={goTest ? testData.guarantorResidence.FIO : regData.secondGuarantName}
                            onChange={(e) => props.setSecondGuarantName(e.target.value)}
                            onFocus={() => props.clearError("guarantorResidence.FIO")}
                            error={errors["guarantorResidence.FIO"]}
                        />
                        <Input
                            mask="+7(999) 999 99 99"
                            type="text"
                            className="registration-step3__form"
                            labelText="+7 ("
                            value={goTest ? testData.guarantorResidence.phone : regData.secondGuarantTelephone}
                            onChange={(e) => props.setSecondGuarantTelephone(e.target.value)}
                            onFocus={() => props.clearError("guarantorResidence.phone")}
                            error={errors["guarantorResidence.phone"]}
                        />
                    </div>
                    <div>
                        <h2 className="registration-step3__form-block-title">Дополнительная информация</h2>
                        <Input
                            type="text"
                            id="skype"
                            className="registration-step3__form"
                            labelText="Skype"
                            value={goTest ? testData.skype : regData.skype}
                            onChange={(e) => props.setSkype(e.target.value)}
                        />
                        <Select
                            title={goTest ? "Высшее" : "Образование"}
                            data={education}
                            dataAtr={"ext_id"}
                            onClick={props.setSelectedEducation}
                            className="registration-step3__form"
                            onFocus={() => props.clearError("educationTypeId")}
                            error={errors.educationTypeId}
                        />
                        <div>
                            <Select
                                title={goTest ? "Холост" : "Семейное положение"}
                                data={maritalStatus}
                                dataAtr={"ext_id"}
                                onClick={props.setSelectedMaritalStatus}
                                className="registration-step3__form"
                                onFocus={() => props.clearError("familyStatusTypeId")}
                                error={errors.familyStatusTypeId}
                            />
                            <Select
                                title={goTest ? "Отдых" : "Цель получения займа"}
                                data={loanPurposes}
                                dataAtr={"ext_id"}
                                onClick={props.setSelectedLoanPurpose}
                                className="registration-step3__form"
                                onFocus={() => props.clearError("loanPurposeTypeId")}
                                error={errors.loanPurposeTypeId}

                            />
                            <Input
                                type="text"
                                id="issuedDate"
                                className="registration-step3__form"
                                labelText="Суммарный ежемесячный доход"
                                value={goTest ? testData.incomePerMonth : regData.incomePerMonth}
                                onChange={(e) => props.setIncomePerMonth(e.target.value)}
                                onFocus={() => props.clearError("incomePerMonth")}
                                error={errors.incomePerMonth}

                            />
                        </div>
                        <div>
                            <h3 className="registration-step3__form-title">Наличие скрытых кредитов</h3>
                            <div className="registration-step3__form-radio-block-wr">
                                <Input
                                    type="radio"
                                    id="openCreditYes"
                                    name="openCredit"
                                    className="registration-step3__form"
                                    labelText="Да"
                                    checked={regData.hasOpenedCredits}
                                    onChange={props.setHasOpenedCredits}
                                    onFocus={() => props.clearError("amountPayMonthlyLoans")}
                                    error={errors.amountPayMonthlyLoans}
                                />
                                <Input
                                    type="radio"
                                    id="openCreditNo"
                                    name="openCredit"
                                    className="registration-step3__form"
                                    labelText="Нет"
                                    checked={!regData.hasOpenedCredits}
                                    onChange={props.setHasOpenedCredits}
                                />

                            </div>
                            {regData.hasOpenedCredits ? showOpenedCredit() : ""}


                        </div>
                        <div>
                            <h3 className="registration-step3__form-title">Наличие скрытых займов</h3>
                            <div className="registration-step3__form-radio-block-wr">
                                <Input
                                    type="radio"
                                    id="hasOpenedLoansYes"
                                    name="openedloan"
                                    className="registration-step3__form"
                                    labelText="Да"
                                    checked={regData.hasOpenedLoans}
                                    onChange={props.setHasOpenedLoans}
                                />
                                <Input
                                    type="radio"
                                    id="hasOpenedLoansNo"
                                    name="openedloan"
                                    className="registration-step3__form"
                                    labelText="Нет"
                                    checked={!regData.hasOpenedLoans}
                                    onChange={props.setHasOpenedLoans}
                                />

                            </div>
                            {regData.hasOpenedLoans ? showOpenedLoans() : ""}

                            <Input
                                type="text"
                                id="amountTotalOtherObligations"
                                className="registration-step3__form"
                                labelText="Иные денежные обязательства"
                                value={goTest ? testData.amountTotalOtherObligations : regData.amountTotalOtherObligations}
                                onChange={(e) => props.setAmountTotalOtherObligations(e.target.value)}
                                onFocus={() => props.clearError("amountTotalOtherObligations")}
                                error={errors.amountTotalOtherObligations}

                            />

                        </div>
                    </div>
                    <div>
                        <h2 className="registration-step3__form-block-title">Информация о работе</h2>
                        <div className="registration-step3__form-checkbox-block-wr">
                            <Input
                                type="checkbox"
                                id="isPensioner"
                                className="registration-step3__form"
                                labelText="Пенсионер"
                                checked={regData.isPensioner}
                                onChange={props.setIsPensioner}
                            />
                            <Input
                                type="checkbox"
                                id="isUnemployed"
                                className="registration-step3__form"
                                labelText="Безработный"
                                checked={regData.isUnemployed}
                                onChange={() => props.setIsUnemployed()}
                            />
                        </div>
                        {/*{!regData.isPensioner ? renderWorkInfo(regData) : " "}*/}
                        {/*{!regData.isUnemployed ? renderWorkInfo(regData) : " "}*/}
                        {!regData.isUnemployed ? renderWorkInfo() : ""}
                        <div className="registration-step3__form-checkbox-block-wr">
                            <Input
                                type="checkbox"
                                id="wasDeclaredAsBankrupt"
                                className="registration-step3__form"
                                labelText="Банкрот"
                                checked={regData.wasDeclaredAsBankrupt}
                                onChange={() => props.setWasDeclaredAsBankrupt()}
                                onFocus={() => props.clearError("wasDeclaredAsBankrupt")}
                                error={errors.wasDeclaredAsBankrupt}
                            />
                            <Input
                                type="checkbox"
                                id="agreedWithBki"
                                className="registration-step3__form"
                                labelText="Согласие БКИ"
                                checked={regData.agreedWithBki}
                                onChange={props.setAgreedWithBki}
                            />
                            <Input
                                type="checkbox"
                                id="agreedWithDdo"
                                className="registration-step3__form"
                                labelText="Согласие ДДО"
                                checked={regData.agreedWithDdo}
                                onChange={props.setAgreedWithDdo}
                            />
                        </div>


                        <Button
                            text="Далее"
                            className="registration-step3__btn-submit"
                            onClick={() => props.submitStepThree()}
                        />
                    </div>
                    <Input
                        type="checkbox"
                        className="registration-step3__form"
                        labelText="Настоящим я подтверждаю, что ознакомлен и согласен со следующим"
                        // onClick={props.runTestStep3}
                        // checked={data.registration.livingByRegAddress}
                        // onChange={(e) => props.setLivingByRegAddress()}
                    />

                </div>
            </div>
        </section>
    )
}
// }

// export default Step3;
