import Input from "../components/pieces/Input";
import * as React from "react";
import {useState} from "react";
import {useEffect} from "react";

//очищает строку оставляя только цифры
export const clearNumberField = (field) => {
    return field.replace(/[^-0-9]/gim, '');
};

//настройки всех слайдеров на всех страницах
//принимает сколько изначально показывать слайдов, и сколько на разрешении
//после 1024px
export const slidersSettings =(base, large) => {
    const [slidesToShow, changeSlidesToShow] = useState(base);
    useEffect(() => {
        let width = window.innerWidth;
        if (width >= 1024) {
            changeSlidesToShow(large);
        }
    });

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: slidesToShow,
        slidesToScroll: 1,
    };
    return settings;
};

// interface TextInput {
//     className: string,
//     id: string,
//     label: string,
//     value: any,
//     error: string,
//     onFocus: any,
//     onChange: any,
//     mask: string,
//
// }

// {className: className,id: id,label: label,value: value, error: error, onFocus:onFocus,onChange: onChange, mask: mask}

export const renderTextInput = ({className: className,id: id,label: label,value: value, error: error, onFocus:onFocus,onChange: onChange, mask: mask}) => {
    return (
        <Input
            mask={mask}
            type="text"
            className={className}
            labelText={label}
            onFocus={()=>onFocus(id)}
            error={error[id]}
            value={value[id]}
            onChange={(e)=>onChange(e.target.value)}
        />
    )
};

export const renderButton = ( {text,className,onClick}) => {
    return (
        <li className={className + "-item"}>
            <button className={className + "-btn"}
                    onClick={onClick}
            >{text}</button>
        </li>
    );
};
