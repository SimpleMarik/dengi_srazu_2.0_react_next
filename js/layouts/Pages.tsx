// import Footer from '../components/Footer';
import HeaderContainer from "../wrappedComponents/HeaderContainer";
import React from "react";
import Footer from "../components/Footer";

// import Route from 'next/router'
export default function Pages({children}) {
    // console.log(Route.asPath);
    // const isClient = typeof document !== 'undefined';
    // console.log(isClient);
    // console.log(process);

    return (
        <>
            <HeaderContainer/>
            {children}
            <Footer/>
        </>
    )
}
