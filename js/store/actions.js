import {TEST, UPDATE_TOKEN} from "./constants";

export const test = () => {
    return {
        type: TEST,
        payload: true
    }
};
export const updateToken = (token) => {
    console.log(token);
    return {
        type: UPDATE_TOKEN,
        payload: token
    }
};

export function setProjectData(name, value) {
    return {
        type: 'SET_PROJECT_DATA',
        payload: {
            name,
            value
        }
    };
}

