import {
    CLEAR_ERROR,
    SET_AUTH_CODE, SET_AUTH_ERRORS, SET_AUTH_PASSWORD,
    SET_AUTH_PHONE, SET_AUTH_SUCCESS, SET_USER_DATA,
} from "../constants";

import Router from 'next/router';
import cookie from 'react-cookies';
import {clearNumberField} from "../../helpers/helpers";
import {authLogin, authLogout, getClient, apiGetOptions} from "../../services/Api";

export const setAuthPhone = (userPhone) => {
    return {
        type: SET_AUTH_PHONE,
        payload: userPhone,
    }
};

export const setAuthPassword = (authPassword) => {
    return {
        type: SET_AUTH_PASSWORD,
        payload: authPassword,
    }
};

export const setAuthCode = (authCode) => {
    return {
        type: SET_AUTH_CODE,
        payload: authCode,
    }
};

export const setAuthSucces = (data) => {
    return {
        type: SET_AUTH_SUCCESS,
        payload: data,
    }
};
export const setUserData = (data) => {
    return {
        type: SET_USER_DATA,
        payload: data,
    }
};

export const setAuthErrors = (errors) => {
    return {
        type: SET_AUTH_ERRORS,
        payload: errors,
    }
};
export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

// export const submitAuth = (field) => {
//     return {
//         type: CLEAR_ERROR,
//         payload: field
//     }
// };

export const submitAuth = () => {
    return (dispatch, getState) => {
        const {auth} = getState();
        const authData = {
            username: clearNumberField(auth.authPhone),
            password: auth.authPassword
        };
        authLogin(authData)
            .then((response) => {
                    console.log(response)

                    if (response.data) {
                        dispatch(setAuthSucces(response));

                        // cookie.save("Auth",response.data,{ maxAge:"6000"});
                        Router.push('/cabinet');
                    }
                }
            )
            .catch((e) => {
                if (e.response) {
                    if (e.response.status === 404) {
                        dispatch(setAuthErrors({phone: ["Пользователь не найден"]}));
                    } else if (e.response.status === 401) {
                        dispatch(setAuthErrors({phone: ["Неверный логин или пароль"]}));
                    } else {
                        dispatch(setAuthErrors(e.response.data.errors));
                    }
                }
            })
    }
};

export const logoutUser = () => {

    return (dispatch, getState) => {
        console.log('logout');
        authLogout()
            .then((response) => {
                console.log(response)
                if(response.data.success) {
                    Router.push('/');
                }
        })
    }
};

export const getOptions = () => {
    return (dispatch) => {
        return apiGetOptions();
    }
}

export const isLogin = () => {

    return (dispatch, getState) => {
        console.log('logout');
        getClient()
            .then((response) => {
            console.log(response.status, "status")
        })
            .catch((error) => {
                console.log(error.response.status, "status catch")
                if(error.response.status) {
                    Router.push("/login")
                }
            })

    }
};
