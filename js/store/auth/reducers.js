import {baseState} from "../store";
import {
    CLEAR_ERROR,
    SET_AUTH_CODE, SET_AUTH_ERRORS, SET_AUTH_PASSWORD,
    SET_AUTH_PHONE,
    SET_AUTH_SUCCESS,
    SET_USER_DATA,
} from "../constants";


export const authReducer = (state = baseState, action) => {
    switch (action.type) {
        case SET_AUTH_PHONE: {
            return {...state, authPhone: action.payload}
        }

        case SET_AUTH_PASSWORD: {
            return {...state, authPassword: action.payload}
        }

        case SET_AUTH_CODE: {
            return {...state, authCode: action.payload}
        }

        case SET_AUTH_SUCCESS: {
            return {
                ...state, authSuccessMessage: action.payload.message,
                authSuccessCode: action.payload.code,
                showAuthEnterCode: !state.showAuthEnterCode
            }
        }
        case SET_USER_DATA: {
            return {...state, userData: action.payload}
        }
        case SET_AUTH_ERRORS: {
            return {...state, authErrors: action.payload}
        }
        case CLEAR_ERROR: {
            return {...state, authErrors: {...state.authErrors, [action.payload]: ""}}
        }
    }
    return state;
};
