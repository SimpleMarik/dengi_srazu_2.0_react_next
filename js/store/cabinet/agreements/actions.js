import { apiGetAgreements, apiGetSelectedAgreements} from "../../../services/Api";
import Input from "../../../components/pieces/Input";
import React from "react";
import {SET_AGREEMENTS} from "../../constants";
import {setProjectData} from "../../actions";

export const setPhoto = (appealFile, appearUrl, id) => {
    return {
        type: "ADD_APPEAL_FILE",
        payload: {
            file: appealFile,
            url: appearUrl,
            id: id
        }
    }
};

export const setInputs = (data) => {
    return {
        type: "ADD_INPUTS_FILE",
        payload: data
    }
};
export const setAgreements = (agreements) => {
    return {
        type: SET_AGREEMENTS,
        payload: agreements
    }
};

export const getAgreements = () => {
    return (dispatch, getState) => {
        apiGetAgreements().then((response) => {
            dispatch(setAgreements(response.data));
        })
    }
};

export const getSelectedAgreements = (id) => {
    return (dispatch) => {
        apiGetSelectedAgreements(id).then(response => {
            dispatch(setProjectData("selectedAgreements", response.data))
        })
    }
}

