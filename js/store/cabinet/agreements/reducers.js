import {baseState} from "../../store";
import {SET_AGREEMENTS} from "../../constants";

export const agreements = (state = baseState, action) => {
    switch (action.type) {
        case "ADD_APPEAL_FILE": {
            return state = {
                ...state, appealFiles: {
                    ...state.appealFiles,
                    [action.payload.id]: action.payload.file,
                    [action.payload.id + "url"]: action.payload.url
                }
            }
        }
        case SET_AGREEMENTS: {
            console.log(action.payload)
            return state = {...state, agreements: action.payload}
        }
        case "ADD_INPUTS_FILE": {
            return state = {...state, fileInputs: action.payload}
        }

    }
    return state;
};
