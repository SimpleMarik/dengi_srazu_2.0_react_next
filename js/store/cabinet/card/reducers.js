import {baseState} from "../../store";
import {
    SET_CAB_CHANGE_PASSWORD_ERRORS, SET_CHANGE_NEW_PASSWORD, SET_CHANGE_NEW_PASSWORD_CONFIRM,
    SET_CHANGE_OLD_PASSWORD
} from "../../constants";

export const changePassword = (state = baseState, action) => {
    switch (action.type) {
        case SET_CHANGE_OLD_PASSWORD: {
            return state = {...state, oldChangePassword: action.payload}
        }
        case SET_CHANGE_NEW_PASSWORD: {
            return state = {...state, newChangePassword: action.payload}
        }
        case SET_CHANGE_NEW_PASSWORD_CONFIRM: {
            return state = {...state, newChangePasswordConfirm: action.payload}
        }
        case SET_CAB_CHANGE_PASSWORD_ERRORS: {
            return state = {...state, changePasswordErrors: action.payload}
        }
    }
    return state;
};
