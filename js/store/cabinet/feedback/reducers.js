import {baseState} from "../../store";

export const feedback = (state = baseState, action) => {
    switch (action.type) {
        case "ADD_APPEAL_FILE": {
            return state = {
                ...state, appealFiles: {
                    ...state.appealFiles,
                    [action.payload.id]: action.payload.file,
                    [action.payload.id + "url"]: action.payload.url
                }
            }
        }
        case "ADD_INPUTS_FILE": {
            return state = {...state, fileInputs: action.payload}
        }

    }
    return state;
};
