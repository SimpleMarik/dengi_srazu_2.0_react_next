import {
    SET_USER_APPLICATION_DATA, SET_USER_LOAN
} from "../../constants";
// import {
//     apiGetClientAgreements, apiGetClientAplication,
//     clientChangePassword,
//     getCabinetOptions, getUserAplicationData,
// } from "../../../services/Api";
import cookie from "react-cookies";


export const setUserLoan = (userLoanList) => {
    return {
        type: SET_USER_LOAN,
        payload: userLoanList,
    }
};
export const setUserApplicationData = (data) => {
    return {
        type: SET_USER_APPLICATION_DATA,
        payload: data,
    }
};

export const submitChangePassword = (e) => {
    console.log(e);
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep5Reducer;

        const extId = registration.regStep4Reducer.stepFourDataReturn.extId;

        console.log(extId);
        const registerData = {
            token: extId,
            oldPassword: data.card.cardNumber,
            newPassword: data.card.cardMonth,
            confirmPassword: data.card.cardYear,
        };
        // clientChangePassword(registerData).then(function (r) {
        //     if (r.data) {
        //         // dispatch(setStep5(r.data));
        //         // Router.push("/six");
        //     }
        // });
    }
};

export const getLoanData = (e) => {
    // getCabinetOptions().then(r => {
    // });
    // console.log(e.getAttribute("data-ext_id"));

    return (dispatch) => {
        let data = {};
        if (process.browser) {
            // const cook = cookie.load("Auth");
            data = {
                // token: cook.token,
                extId: e.getAttribute("data-ext_id")
            }
        }

        // getUserAplicationData(data)
        //     .then((r) => {
        //             if (r.data) {
        //                 console.log(r.data);
        //                 dispatch(setUserApplicationData(r.data))
        //             }
        //         }
        //     )
        //     .catch((e) => {
        //         if (e.response) {
        //             console.log(e.response.error)
        //         }
        //     })
    }
};

export const getClientAgreements = () => {
    // getCabinetOptions().then(r => {
    // });
    return (dispatch) => {
        let data = {};
        if (process.browser) {
            const cook = cookie.load("Auth");
            data = {
                token: cook.token
            }
        }
        // apiGetClientAgreements(data)
        //     .then((r) => {
        //             if (r.data) {
        //                 console.log(r.data.length);
        //                 dispatch(getUserAplication());
        //             }
        //         }
        //     )
        //     .catch((e) => {
        //         if (e.response) {
        //             console.log(e.response.error)
        //         }
        //     })
    }
};
export const getUserAplication = () => {
    return (dispatch) => {
        let data = {};
        if (process.browser) {
            const cook = cookie.load("Auth");
            data = {
                token: cook.token
            }
        }
        // apiGetClientAplication(data)
        //     .then((r) => {
        //             if (r.data) {
        //                 // dispatch(setUserLoan(r.data))
        //                 console.log(r.data)
        //             }
        //         }
        //     )
        //     .catch((e) => {
        //         if (e.response) {
        //             console.log(e.response.error)
        //         }
        //     })
    }
};
