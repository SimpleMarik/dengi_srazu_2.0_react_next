import {
    SET_CALC_VALUE, SET_DEFAULT_DATA, SET_PROMOCODE
} from "../constants";
import {apiGetCalcSetting, getCalcSetting} from "../../services/Api";
import moment from "moment";
import {setProjectData} from "../actions";

export const setPromocode = (promocode) => {
    return {
        type: SET_PROMOCODE,
        payload: promocode
    }
};

export const setDefaultData = (defaultData) => {
    return {
        type: SET_DEFAULT_DATA,
        payload: defaultData,
    }
};

export const setCalcValue = (returnDate, returnSum, defaultSum) => {
    return {
        type: SET_CALC_VALUE,
        payload: {
            returnDate: returnDate,
            returnSum: returnSum,
            defaultSum: defaultSum
        },
    }
};

export const _getDefaultData = () => {
    return (dispatch) => {
        // apiGetCalcSetting()
        //     .then(function (r) {
        //         if (r.data) {
        //             r.data.child.map((element) => {
        //                 dispatch(setProjectData(element.comp, element.props));
        //             });
        //         }
        //     });
    }
};

export const setCalcSum = (data) => {
    return (dispatch, getState) => {
        const {base} = getState();
        const {baseCalc} = base;
        const {defaultCalc} = base;
        let currentDay = baseCalc !== undefined ? baseCalc.currentDay : defaultCalc.currentDay;
        const percent = baseCalc !== undefined ? baseCalc.percent : defaultCalc.percent;
        const selectedSum = base.hasOwnProperty("selectedSum") ?
            base.selectedSum : base.hasOwnProperty("baseCalc") ? base.baseCalc.currentSum : defaultCalc.currentSum
        let products;
        let defaultSum;


        // if (typeof data === "object") {
        //     dispatch(setDefaultData(data));
        //     products = data.products[0];
        //     defaultSum = data.defaultSum;
        //     defaultData = data;
        // } else {
        //     products = calc.defaultCalcData.products[0];
        //     defaultData = calc.defaultCalcData;
        //     defaultSum = data
        let returnSum = parseInt(selectedSum) * (parseInt(currentDay) - 1) * parseFloat(percent);

        dispatch(setProjectData("selectedSum", data ? data : selectedSum));
        dispatch(setProjectData("returnSum", returnSum));
        let date = moment().add(currentDay - 1, 'days');
        let returnDate = moment(date).format('DD.MM.YYYY');
        console.log(returnDate);
        dispatch(setProjectData("returnDate", returnDate));
        // dispatch(setCalcValue(returnDate, returnSum, defaultSum));
    }
};
