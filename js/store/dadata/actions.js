import {
    DADATA_CITY,
    DADATA_SEARCH_AREA,
    DADATA_SEARCH_CITY, DADATA_SEARCH_HOUSE_NUMBER,
    DADATA_SEARCH_REGION, DADATA_SEARCH_SETTLEMENT,
    DADATA_SEARCH_STREET
} from "../constants";
// import {dadataGetData} from "../../services/Api";
export const dadataSearchRegion = (searchRegion) => {
    return {
        type: DADATA_SEARCH_REGION,
        payload: {
            regionList: searchRegion,
            // regionId: searchRegion.suggestions[0].data.area_fias_id
        }
    }
};


export const dadataSearchArea = (searchArea) => {
    return {
        type: DADATA_SEARCH_AREA,
        payload: {
            areaList: searchArea,
            // areaId: searchArea.suggestions[0].data.area_fias_id
        }
    }
};
export const dadataCity = (city) => {
    return {
        type: DADATA_CITY,
        payload: city
    }
};




export const dadataSearchCity = (searchCity) => {
    return {
        type: DADATA_SEARCH_CITY,
        payload: searchCity
    }
};

export const dadataSearchSettlement = (searchSettlement) => {
    return {
        type: DADATA_SEARCH_SETTLEMENT,
        payload: searchSettlement
    }
};

export const dadataSearchStreet = (searchStreet) => {
    return {
        type: DADATA_SEARCH_STREET,
        payload: searchStreet
    }
};
export const dadataSearchHouseNumber = (searchHouseNumber) => {
    return {
        type: DADATA_SEARCH_HOUSE_NUMBER,
        payload: searchHouseNumber
    }
};
export const getRegion = (value) => {
    return (dispatch) => {
        const data = {
            query: value,
            from_bound: {value: "region"},
            to_bound: {value: "region"},
            count: "10",
            bounds: "region-area"
        };
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchRegion(r));
        // })
    }
};

export const getArea = (value) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        console.log(registration);
        const data = {
            query: value,
            count: "10",
            from_bound: {value: "area"},
            to_bound: {value: "area"},
            locations: [{region_fias_id: registration.regStep2Reducer.region_fias_id}],
            restrict_value: true
        };
        console.log(registration.regStep2Reducer.region_fias_id);
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchArea(r));
        // })
    }
};

export const getCity = (value) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const searchData = registration.regStep2Reducer;
        const data = {
            query: value,
            count: "10",
            from_bound: {value: "city"},
            to_bound: {value: "city"},
            locations: [searchData.area_fias_id ?
                {area_fias_id: searchData.area_fias_id} :
                {region_fias_id: searchData.region_fias_id}
            ],
            restrict_value: true
        };
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchCity(r));
        // })
    }
};

export const getSettlement = (value) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        console.log(registration)
        const searchData = registration.regStep2Reducer;
        const data = {
            query: value,
            count: "10",
            from_bound: {value: "settlement"},
            to_bound: {value: "settlement"},
            locations: [searchData.city_fias_id ?
                {city_fias_id: searchData.city_fias_id} :
                searchData.area_fias_id ?
                    {area_fias_id: searchData.area_fias_id} :
                    {region_fias_id: searchData.region_fias_id}],
            restrict_value: true
        };
        console.log(data);
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchSettlement(r));
        // })
    }
};

export const getStreet = (value) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = {
            query: value,
            count: "10",
            from_bound: {value: "street"},
            to_bound: {value: "street"},
            locations: [registration.regStep2Reducer.settlement_fias_id ?
                {settlement_fias_id: registration.regStep2Reducer.settlement_fias_id} :
                {city_fias_id: registration.regStep2Reducer.city_fias_id}],
            restrict_value: true
        };
        console.log(registration.regStep2Reducer.settlement_fias_id);
        console.log(registration.regStep2Reducer.city_fias_id);
        console.log(data);
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchStreet(r));
        // })
    }
};

export const getHouseNumber = (value) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = {
            query: value,
            count: "10",
            from_bound: {value: "house"},
            locations: [registration.regStep2Reducer.street_fias_id ?
                {street_fias_id: registration.regStep2Reducer.street_fias_id}
                : {settlement_fias_id: registration.regStep2Reducer.settlement_fias_id}],
            restrict_value: true
        };
        // dadataGetData(data).then((r) => {
        //     dispatch(dadataSearchHouseNumber(r));
        // })
    }
};


