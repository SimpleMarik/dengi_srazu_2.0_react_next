import {
    RUN_TEST_STEP5, SET_CONFIRM_PASSWORD, SET_FINAL_USER_DATA,
    SET_PASSWORD
} from "../../constants";
// import {setOrChangePassword} from "../../../services/Api";
import Router from "next/router";

export const runTestStep5 = () => {
    return {
        type: RUN_TEST_STEP5,
    }
};

export const setPassword = (password) => {
    return {
        type: SET_PASSWORD,
        payload: password,
    }
};

export const setConfirmPassword = (confirmPassword) => {
    return {
        type: SET_CONFIRM_PASSWORD,
        payload: confirmPassword,
    }
};
export const setFinalUserData = (finalUserData) => {
    return {
        type: SET_FINAL_USER_DATA,
        payload: finalUserData,q
    }
};
export const submitFinalStep = () => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regFinalStepReducer;
        const token = registration.regStep4Reducer.userAuthToken;
        const registerData = {
            token: token,
            newPassword: data.password,
            confirmPassword: data.confirmPassword
        };
        // setOrChangePassword(registerData).then(function (r) {
        //     if (r.data) {
        //         dispatch(setFinalUserData(r.data));
        //         Router.push("/cabinet");
        //     }
        // });
    }
};





