// import Router from 'next/router';
import {baseState} from "../../store";
import {
    CLEAR_ERROR,
    RUN_TEST,
    SET_BIRTHDAY_DATE,
    SET_CONSENT_PERSONAL_DATA,
    SET_GENDER,
    SET_NAME,
    SET_PATRONYMIC,
    SET_PHONE,
    SET_SECOND_NAME,
    SET_STEP, SET_STEP1_ERRORS,
    SET_SUBSCRIBE,
} from "../../constants";
import React from "react";

export const regStep1Reducer = (state = baseState, action) => {
    switch (action.type) {

        case RUN_TEST: {
            console.log("test");
            return state = {...state, goTest: !state.goTest}
        }

        case SET_STEP: {
            console.log("step ++");
            return state = {...state, step: state.step++, stepOneDataReturn: action.payload}
        }

        case SET_PHONE: {
            return state = {...state, phone: action.payload}
        }

        case SET_NAME: {
            console.log("setName");
            return {...state, firstName: action.payload}
        }

        case SET_SECOND_NAME: {
            return {...state, lastName: action.payload}
        }

        case SET_PATRONYMIC: {
            return {...state, middleName: action.payload}
        }

        case SET_BIRTHDAY_DATE: {
            return {...state, birthDate: action.payload}
        }

        case SET_GENDER: {
            return {...state, userGender: action.payload}
        }

        case SET_SUBSCRIBE: {
            return {...state, subscribe: !state.subscribe}
        }

        case SET_CONSENT_PERSONAL_DATA: {
            return {...state, consentPersonalData: !state.consentPersonalData}
        }
        case SET_STEP1_ERRORS: {
            return {...state, step1Errors: action.payload}
        }
        case CLEAR_ERROR: {
            return {...state, step1Errors: {...state.step1Errors, [action.payload]: ""}}
        }
    }
    return state;
};
