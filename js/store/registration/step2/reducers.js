import {
    CLEAR_ERROR,
    RUN_TEST_STEP2,
    SET_ACT_AREA,
    SET_ACT_BLOCK_NUMBER,
    SET_ACT_CITY,
    SET_ACT_DATE_REGISTRATION,
    SET_ACT_FLAT_NUMBER,
    SET_ACT_FLAT_POSTAL_CODE,
    SET_ACT_HOUSE_NUMBER,
    SET_ACT_REGION,
    SET_ACT_SETTLEMENT,
    SET_ACT_STREET,
    SET_AREA,
    SET_BIRTH_PLACE,
    SET_BLOCK_NUMBER,
    SET_CITY,
    SET_DATE_REGISTRATION,
    SET_FLAT_NUMBER,
    SET_FLAT_POSTAL_CODE,
    SET_HOUSE_NUMBER,
    SET_ISSUED_BY,
    SET_ISSUED_DATE,
    SET_ISSUED_SUBDIVISION,
    SET_LIVING_BY_REG_ADDRES,
    SET_PASSPORT_NUMBER,
    SET_REGION, SET_SELECTED_ACT_AREA,
    SET_SELECTED_ACT_CITY,
    SET_SELECTED_ACT_HOUSE_NUMBER, SET_SELECTED_ACT_REGION,
    SET_SELECTED_ACT_SETTLEMENT,
    SET_SELECTED_ACT_STREET,
    SET_SELECTED_AREA,
    SET_SELECTED_CITY,
    SET_SELECTED_HOUSE_NUMBER,
    SET_SELECTED_REGION,
    SET_SELECTED_SETTLEMENT,
    SET_SELECTED_STREET,
    SET_SETTLEMENT,
    SET_SNILS,
    SET_STEP2,
    SET_STEP2_ERRORS,
    SET_STREET,
} from "../../constants";
import {baseState} from "../../store";

export const regStep2Reducer = (state = baseState, action) => {
    switch (action.type) {
        case RUN_TEST_STEP2: {
            return state = {...state, goTest: !state.goTest}
        }
        case SET_STEP2: {
            return state = {...state, stepTwoDataReturn: action.payload}
        }
        case SET_PASSPORT_NUMBER: {
            return {...state, passportSeries: action.payload.series, passportNumber: action.payload.number}
        }
        case SET_ISSUED_BY: {
            return {...state, issuedBy: action.payload}
        }
        case SET_ISSUED_DATE: {
            return {...state, issuedDate: action.payload}
        }
        case SET_ISSUED_SUBDIVISION: {
            return {...state, issuedSubdivision: action.payload}
        }
        case SET_BIRTH_PLACE: {
            return {...state, birthPlace: action.payload}
        }
        case SET_SNILS: {
            return {...state, snils: action.payload}
        }
        case SET_REGION: {
            return {...state, addressReg: {...state.addressReg, region: action.payload}, showRegionHint: true}
        }
        case SET_AREA: {
            return {...state, addressReg: {...state.addressReg, area: action.payload}, showAreaHint: true}
        }
        case SET_CITY: {
            return {...state, addressReg: {...state.addressReg, city: action.payload}, showCityHint: true}
        }
        case SET_SETTLEMENT: {
            return {...state, addressReg: {...state.addressReg, settlement: action.payload}, showSettlementHint: true}
        }
        case SET_STREET: {
            return {...state, addressReg: {...state.addressReg, street: action.payload}, showStreetHint: true}
        }
        case SET_HOUSE_NUMBER: {
            return {...state, addressReg: {...state.addressReg, houseNumber: action.payload}, showHouseNumberHint: true}
        }
        case SET_BLOCK_NUMBER: {
            return {...state, addressReg: {...state.addressReg, blockNumber: action.payload}}
        }
        case SET_FLAT_NUMBER: {
            return {...state, addressReg: {...state.addressReg, flatNumber: action.payload}}
        }
        case SET_FLAT_POSTAL_CODE: {
            return {...state, addressReg: {...state.addressReg, postalCode: action.payload}}
        }
        case SET_DATE_REGISTRATION: {
            return {...state, addressReg: {...state.addressReg, dateRegistration: action.payload}}
        }
        case SET_ACT_REGION: {
            return {...state, addressAct: {...state.addressAct, region: action.payload}, showRegionHint: true}
        }
        case SET_ACT_AREA: {
            return {...state, addressAct: {...state.addressAct, area: action.payload}, showAreaHint: true}
        }
        case SET_ACT_CITY: {
            return {...state, addressAct: {...state.addressAct, city: action.payload}, showCityHint: true}
        }
        case SET_ACT_SETTLEMENT: {
            return {...state, addressAct: {...state.addressAct, settlement: action.payload}, showSettlementHint: true}
        }
        case SET_ACT_STREET: {
            return {...state, addressAct: {...state.addressAct, street: action.payload}, showStreetHint: true}
        }
        case SET_ACT_HOUSE_NUMBER: {
            return {...state, addressAct: {...state.addressAct, houseNumber: action.payload}, showHouseNumberHint: true}
        }
        case SET_ACT_BLOCK_NUMBER: {
            return {...state, addressAct: {...state.addressAct, blockNumber: action.payload}}
        }
        case SET_ACT_FLAT_NUMBER: {
            return {...state, addressAct: {...state.addressAct, flatNumber: action.payload}}
        }
        case SET_ACT_FLAT_POSTAL_CODE: {
            return {...state, addressAct: {...state.addressAct, postalCode: action.payload}}
        }
        case SET_ACT_DATE_REGISTRATION: {
            return {...state, addressAct: {...state.addressAct, dateRegistration: action.payload}}
        }
        case SET_LIVING_BY_REG_ADDRES: {
            return {...state, livingByRegAddress: !state.livingByRegAddress}
        }
        case SET_SELECTED_REGION: {
            return {
                ...state,
                addressReg: {...state.addressReg, region: action.payload.text},
                region_fias_id: action.payload.data,
                showRegionHint: false
            }
        }
        case SET_SELECTED_AREA: {
            return {
                ...state,
                addressReg: {...state.addressReg, area: action.payload.text},
                area_fias_id: action.payload.data,
                showAreaHint: false
            }
        }
        case SET_SELECTED_CITY: {

            return {
                ...state,
                addressReg: {...state.addressReg, city: action.payload.text, postalCode: action.payload.postalCode},
                city_fias_id: action.payload.data,
                showCityHint: false
            }
        }
        case SET_SELECTED_SETTLEMENT: {
            return {
                ...state,
                addressReg: {...state.addressReg, settlement: action.payload.text},
                settlement_fias_id: action.payload.data,
                showSettlementHint: false
            }
        }
        case SET_SELECTED_STREET: {
            return {
                ...state,
                addressReg: {...state.addressReg, street: action.payload.text},
                street_fias_id: action.payload.data,
                showStreetHint: false
            }
        }
        case SET_SELECTED_HOUSE_NUMBER: {
            return {
                ...state,
                addressReg: {...state.addressReg, houseNumber: action.payload.text},
                showHouseNumberHint: false
            }
        }
        case SET_SELECTED_ACT_REGION: {
            return {
                ...state,
                addressAct: {...state.addressAct, region: action.payload.text},
                act_region_fias_id: action.payload.data,
                showRegionHint: false
            }
        }
        case SET_SELECTED_ACT_AREA: {
            return {
                ...state,
                addressAct: {...state.addressAct, area: action.payload.text},
                act_area_fias_id: action.payload.data,
                showAreaHint: false
            }
        }
        case SET_SELECTED_ACT_CITY: {

            return {
                ...state,
                addressAct: {...state.addressAct, city: action.payload.text, postalCode: action.payload.postalCode},
                act_city_fias_id: action.payload.data,
                showCityHint: false
            }
        }
        case SET_SELECTED_ACT_SETTLEMENT: {
            return {
                ...state,
                addressAct: {...state.addressAct, settlement: action.payload.text},
                act_settlement_fias_id: action.payload.data,
                showSettlementHint: false
            }
        }
        case SET_SELECTED_ACT_STREET: {
            return {
                ...state,
                addressAct: {...state.addressAct, street: action.payload.text},
                act_street_fias_id: action.payload.data,
                showStreetHint: false
            }
        }
        case SET_SELECTED_ACT_HOUSE_NUMBER: {
            return {
                ...state,
                addressReg: {...state.addressReg, houseNumber: action.payload.text},
                showHouseNumberHint: false
            }
        }
        case SET_STEP2_ERRORS: {
            return {
                ...state,step2Errors: action.payload }
        }
        case CLEAR_ERROR: {
            return  {...state, step2Errors: {...state.step2Errors, [action.payload]: ""}}
        }
    }
    return state;
};
