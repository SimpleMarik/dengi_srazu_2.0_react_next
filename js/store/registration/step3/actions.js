import {
    CLEAR_ERROR,
    RUN_TEST_STEP3, SET_AGREE_WITH_BKI, SET_AGREE_WITH_DDO,
    SET_AMOUNT_PAY_MONTHLY_CREDITS,
    SET_AMOUNT_PAY_MONTHLY_LOANS,
    SET_AMOUNT_TOTAL_OTHER_OBLIGATIONS, SET_DATA_STEP3,
    SET_DIRECTORY_DATA,
    SET_FIRST_GUARANT_NAME,
    SET_FIRST_GUARANT_TELEPHONE,
    SET_HAS_OPENED_CREDITS,
    SET_HAS_OPENED_LOANS,
    SET_INCOME_PER_MONTH,
    SET_IS_PENSIONER, SET_IS_UNEMPLOYED,
    SET_PHONE_WORKS,
    SET_POSITION_NAME,
    SET_SECOND_GUARANT_NAME,
    SET_SECOND_GUARANT_TELEPHONE,
    SET_SELECTED_EDUCATION,
    SET_SELECTED_GUARANTOR_INCOME,
    SET_SELECTED_GUARANTOR_RESIDENCE,
    SET_SELECTED_LAST_WORK_LENGTH,
    SET_SELECTED_LOAN_PURPOSE,
    SET_SELECTED_MARITAL_STATUS,
    SET_SKYPE, SET_STEP3_ERRORS,
    SET_WAS_DECLARED_AS_BANKRUPT,
    SET_WORK_ADDRESS,
    SET_WORK_NAME,
} from "../../constants";
import {clearNumberField} from "../../../helpers/helpers";
import {
    getEducation,
    getGuarantorIncome,
    getGuarantorResidence, getLastWorkLength, getLoanPurpose,
    getMaritalStatus,
    getOptions,
    registerStepThree
} from "../../../services/Api";
import Router from "next/router";

export const runTestStep3 = () => {
    return {
        type: RUN_TEST_STEP3,
    }
};
// export const setMaGuarantorIncome = (guarantorIncome) => {
//     return {
//         type: SET_GUARANTOR_INCOME,
//         payload: guarantorIncome
//     }
// };
// export const setMaGuarantorResidance = (guarantorResidence) => {
//     return {
//         type: SET_GUARANTOR_RESIDENCE,
//         payload: guarantorResidence
//     }
// };
//
// export const setMaRitalStatus = (maritalStatuses) => {
//     return {
//         type: SET_MARITAL_STATUSES,
//         payload: maritalStatuses
//     }
// };
//
// export const setEducation = (educations) => {
//
//     return {
//         type: SET_EDUCATIONS,
//         payload: educations
//     }
// };
//
// export const setLoanPurpose = (loanPurposes) => {
//     return {
//         type: SET_LOAN_PURPOSES,
//         payload: loanPurposes,
//     }
// };
//
// export const setLastWorkLength = (lastWorkLength) => {
//     return {
//         type: SET_LAST_WORK_LENGTH,
//         payload: lastWorkLength
//     }
// };

export const setDirectoryData = (data) => {
    // console.log(data);
    return {
        type: SET_DIRECTORY_DATA,
        payload: {
            guarantorIncome: data.guarantorIncome,
            guarantorResidence: data.guarantorResidence,
            maritalStatus: data.maritalStatus,
            education: data.education,
            loanPurpose: data.loanPurpose,
            lastWorkLength: data.lastWorkLength,
        }
    }
};


export const setSelectedGuarantorIncome = (guarantorIncome) => {
    return {
        type: SET_SELECTED_GUARANTOR_INCOME,
        payload: guarantorIncome.getAttribute("ext_id"),
    }
};
export const setSelectedGuarantorResidence = (guarantorresidence) => {
    return {
        type: SET_SELECTED_GUARANTOR_RESIDENCE,
        payload: guarantorresidence.getAttribute("ext_id"),
    }
};


export const setSelectedEducation = (education) => {
    return {
        type: SET_SELECTED_EDUCATION,
        payload: education.getAttribute("ext_id"),
    }
};
export const setSelectedMaritalStatus = (maritalStatus) => {
    return {
        type: SET_SELECTED_MARITAL_STATUS,
        payload: maritalStatus.getAttribute("ext_id"),
    }
};
export const setSelectedLoanPurpose = (loanPurpose) => {
    return {
        type: SET_SELECTED_LOAN_PURPOSE,
        payload: loanPurpose.getAttribute("ext_id"),
    }
};

export const setSelectedLastWorkLength = (lastWorkLength) => {
    return {
        type: SET_SELECTED_LAST_WORK_LENGTH,
        payload: lastWorkLength.getAttribute("ext_id"),
    }
};

export const setSkype = (skype) => {
    return {
        type: SET_SKYPE,
        payload: skype,
    }
};

export const setFirstGuarantName= (firstGuarantName) => {
    return {
        type: SET_FIRST_GUARANT_NAME,
        payload: firstGuarantName,
    }
};
export const setFirstGuarantTelephone = (firstGuarantTelephone) => {
    return {
        type: SET_FIRST_GUARANT_TELEPHONE,
        payload: firstGuarantTelephone,
    }
};
export const setSecondGuarantName = (secondGuarantName) => {
    return {
        type: SET_SECOND_GUARANT_NAME,
        payload: secondGuarantName,
    }
};

export const setSecondGuarantTelephone = (secondGuarantTelephone) => {
    return {
        type: SET_SECOND_GUARANT_TELEPHONE,
        payload: secondGuarantTelephone,
    }
};


export const setIncomePerMonth = (incomePerMont) => {
    return {
        type: SET_INCOME_PER_MONTH,
        payload: incomePerMont,
    }
};
export const setHasOpenedCredits = () => {
    return {
        type: SET_HAS_OPENED_CREDITS,
    }
};
export const setAmountPayMonthlyCredits = (amountPayMonthlyCredits) => {
    return {
        type: SET_AMOUNT_PAY_MONTHLY_CREDITS,
        payload: amountPayMonthlyCredits,
    }
};
export const setHasOpenedLoans = () => {
    return {
        type: SET_HAS_OPENED_LOANS,
    }
};
export const setAmountPayMonthlyLoans = (amountPayMonthlyLoans) => {
    return {
        type: SET_AMOUNT_PAY_MONTHLY_LOANS,
        payload: amountPayMonthlyLoans,
    }
};
export const setAmountTotalOtherObligations = (amountTotalOtherObligations) => {
    return {
        type: SET_AMOUNT_TOTAL_OTHER_OBLIGATIONS,
        payload: amountTotalOtherObligations,
    }
};

export const setIsPensioner= (issPensioner) => {
    return {
        type: SET_IS_PENSIONER,
        payload: issPensioner,
    }
};

export const setIsUnemployed = (isUnemployed) => {
    return {
        type: SET_IS_UNEMPLOYED,
        payload: isUnemployed,
    }
};
export const setWorkName = (workName) => {
    return {
        type: SET_WORK_NAME,
        payload: workName,
    }
};
export const setPositionName = (positionName) => {
    return {
        type: SET_POSITION_NAME,
        payload: positionName,
    }
};
export const setWorkAddress = (workAddress) => {
    return {
        type: SET_WORK_ADDRESS,
        payload: workAddress,
    }
};
export const setPhoneWorks = (phoneWorks) => {
    return {
        type: SET_PHONE_WORKS,
        payload: phoneWorks,
    }
};
export const setWasDeclaredAsBankrupt = () => {
    return {
        type: SET_WAS_DECLARED_AS_BANKRUPT,
    }
};
export const setAgreedWithDdo = () => {
    return {
        type: SET_AGREE_WITH_DDO
    }
};
export const setAgreedWithBki = () => {
    return {
        type: SET_AGREE_WITH_BKI,
    }
};

export const setStep3 = (data) => {
    return {
        type: SET_DATA_STEP3,
        payload: data,
    }
};

export const setStep3Errors = (errors) => {
    return {
        type: SET_STEP3_ERRORS,
        payload: errors
    }
};

export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

export const submitStepThree = (history) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep3Reducer;
        const goTest = registration.regStep3Reducer.goTest;
        const testData = data.testData;
        const extId = registration.regStep2Reducer.stepTwoDataReturn.extId;
        const registerData = {
            extId:  extId,
            skype: goTest ? testData.skype : data.skype,
            educationTypeId: goTest ? testData.educationTypeId : data.selectedEducationId,
            familyStatusTypeId: goTest ? testData.familyStatusTypeId : data.selectedMaritalId,
            loanPurposeTypeId: goTest ? testData.loanPurposeTypeId : data.selectedLoanPurposesId,
            incomePerMonth: goTest ? testData.incomePerMonth : data.incomePerMonth,
            hasOpenedCredits: goTest ? testData.hasOpenedCredits : data.hasOpenedCredits,
            amountPayMonthlyCredits: goTest ? testData.amountPayMonthlyCredits : data.amountPayMonthlyCredits,
            hasOpenedLoans: goTest ? testData.hasOpenedLoans : data.hasOpenedLoans,
            amountPayMonthlyLoans: goTest ? testData.amountPayMonthlyLoans : data.amountPayMonthlyLoans,
            amountTotalOtherObligations: goTest ? testData.amountTotalOtherObligations : data.amountTotalOtherObligations,
            isPensioner: goTest ? testData.isPensioner : data.isPensioner,
            isUnemployed: goTest ? testData.isUnemployed : data.isUnemployed,
            workName: goTest ? testData.workName : data.workName,
            positionName: goTest ? testData.positionName : data.positionName,
            workAddress: goTest ? testData.workAddress : data.workAddress,
            workLengthLastTypeId: goTest ? testData.workLengthLastTypeId : data.selectedLastWorkLengthId,
            phoneWork: goTest ? testData.phoneWork : data.phoneWork,
            wasDeclaredAsBankrupt: goTest ? testData.wasDeclaredAsBankrupt : data.wasDeclaredAsBankrupt,
            guarantorIncome: {
                extId: goTest ? testData.guarantorIncome.extId : data.selectedGuarantorIncome,
                FIO: goTest ? testData.guarantorIncome.FIO : data.firstGuarantName,
                phone: goTest ? testData.guarantorIncome.phone : clearNumberField(data.firstGuarantTelephone)
            },
            guarantorResidence: {
                extId: goTest ? testData.guarantorResidence.extId : data.selectedGuarantorResidence,
                FIO: goTest ? testData.guarantorResidence.FIO : data.secondGuarantName,
                phone: goTest ? testData.guarantorResidence.phone : clearNumberField(data.secondGuarantTelephone)
            },
            agreedWithBki: goTest ? testData.agreedWithBki : data.agreedWithBki,
            agreedWithDdo: goTest ? testData.agreedWithDdo : data.agreedWithDdo,
        };
        // registerStepThree(registerData)
        //     .then(function (r) {
        //         if (r.data) {
        //             dispatch(setStep3(r.data));
        //             Router.push("/four");
        //         }
        //     })
        //     .catch((e) => {
        //         if (e.response) {
        //             dispatch(setStep3Errors(e.response.data));
        //         }
        //     })
    }
};

export const getDefaultDataStep3 = () => {
    // getOptions().then(r => {
    // });
    return (dispatch) => {
        let data = {
            education: {}
        };
        // getGuarantorIncome().then(function (r) {
        //     if (r) {
        //         data.guarantorIncome = r;
        //         // dispatch(setMaGuarantorIncome(r));
        //     }
        // });
        // getGuarantorResidence().then(function (r) {
        //     if (r) {
        //         data.guarantorResidence = r;
        //         // dispatch(setMaGuarantorResidance(r));
        //     }
        // });
        // getMaritalStatus().then(function (r) {
        //     if (r) {
        //         data.maritalStatus = r;
        //         // dispatch(setMaRitalStatus(r));
        //     }
        // });
        // getEducation().then(function (r) {
        //     if (r) {
        //         data.education = r;
        //         // console.log(data);
        //         // dispatch(setEducation(r));
        //     }
        // });
        // getLoanPurpose().then(function (r) {
        //     if (r) {
        //         data.loanPurpose = r;
        //         // dispatch(setLoanPurpose(r));
        //     }
        // });
        // getLastWorkLength().then(function (r) {
        //     if (r) {
        //         data.lastWorkLength = r;
        //         // dispatch(setLastWorkLength(r));
        //         dispatch(setDirectoryData(data));
        //     }
        // });
    };
};
