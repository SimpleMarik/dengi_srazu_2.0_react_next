import {baseState} from "../../store";
import {SET_SELECTED_EDUCATION} from "../../constants";
import {SET_SELECTED_LAST_WORK_LENGTH} from "../../constants";
import {SET_SELECTED_LOAN_PURPOSE} from "../../constants";
import {SET_SELECTED_MARITAL_STATUS} from "../../constants";
import {SET_SELECTED_GUARANTOR_RESIDENCE} from "../../constants";
import {SET_SELECTED_GUARANTOR_INCOME} from "../../constants";
import {SET_SKYPE} from "../../constants";
import {SET_DIRECTORY_DATA} from "../../constants";
import {SET_FIRST_GUARANT_NAME} from "../../constants";
import {SET_FIRST_GUARANT_TELEPHONE} from "../../constants";
import {SET_SECOND_GUARANT_NAME} from "../../constants";
import {SET_SECOND_GUARANT_TELEPHONE} from "../../constants";
import {SET_INCOME_PER_MONTH} from "../../constants";
import {SET_HAS_OPENED_CREDITS} from "../../constants";
import {SET_AMOUNT_PAY_MONTHLY_CREDITS} from "../../constants";
import {SET_HAS_OPENED_LOANS} from "../../constants";
import {SET_AMOUNT_PAY_MONTHLY_LOANS} from "../../constants";
import {SET_AMOUNT_TOTAL_OTHER_OBLIGATIONS} from "../../constants";
import {SET_IS_PENSIONER} from "../../constants";
import {SET_IS_UNEMPLOYED} from "../../constants";
import {SET_WORK_NAME} from "../../constants";
import {SET_POSITION_NAME} from "../../constants";
import {SET_WORK_ADDRESS} from "../../constants";
import {SET_PHONE_WORKS} from "../../constants";
import {SET_WAS_DECLARED_AS_BANKRUPT} from "../../constants";
import {RUN_TEST_STEP3} from "../../constants";
import {SET_DATA_STEP3} from "../../constants";
import {SET_AGREE_WITH_DDO} from "../../constants";
import {SET_AGREE_WITH_BKI} from "../../constants";
import {SET_STEP3_ERRORS} from "../../constants";
import {CLEAR_ERROR} from "../../constants";

export const regStep3Reducer = (state = baseState, action) => {
    switch (action.type) {

        case RUN_TEST_STEP3: {
            return state = {...state, goTest: true}
        }

        // case SET_GUARANTOR_INCOME: {
        //     return state = {...state, guarantorIncome: action.payload}
        // }
        // case SET_GUARANTOR_RESIDENCE: {
        //     return state = {...state, guarantorResidence: action.payload}
        // }
        //
        // case SET_MARITAL_STATUSES: {
        //     return state = {...state, maritalStatus: action.payload}
        // }
        //
        // case SET_EDUCATIONS: {
        //     return state = {...state, education: action.payload}
        // }
        //
        // case SET_LOAN_PURPOSES: {
        //     return {...state, loanPurpose: action.payload}
        // }
        // case SET_LAST_WORK_LENGTH: {
        //     return {...state, lastWorkLength: action.payload}
        // }
        case SET_DIRECTORY_DATA: {
            return {
                ...state,
                guarantorIncome: action.payload.guarantorIncome,
                guarantorResidence: action.payload.guarantorResidence,
                maritalStatus: action.payload.maritalStatus,
                education: action.payload.education,
                loanPurpose: action.payload.loanPurpose,
                lastWorkLength: action.payload.lastWorkLength,
            }
        }
        case SET_SELECTED_GUARANTOR_INCOME: {
            return {...state, selectedGuarantorIncome: action.payload}
        }
        case SET_SELECTED_GUARANTOR_RESIDENCE: {
            return {...state, selectedGuarantorResidence: action.payload}
        }

        case SET_SELECTED_EDUCATION: {
            return {...state, selectedEducationId: action.payload}
        }
        case SET_SELECTED_MARITAL_STATUS: {
            return {...state, selectedMaritalId: action.payload}
        }
        case SET_SELECTED_LOAN_PURPOSE: {
            return {...state, selectedLoanPurposesId: action.payload}
        }
        case SET_SELECTED_LAST_WORK_LENGTH: {
            return {...state, selectedLastWorkLengthId: action.payload}
        }
        case SET_FIRST_GUARANT_NAME: {
            return {...state, firstGuarantName: action.payload}
        }
        case SET_FIRST_GUARANT_TELEPHONE: {
            return {...state, firstGuarantTelephone: action.payload}
        }
        case SET_SECOND_GUARANT_NAME: {
            return {...state, secondGuarantName: action.payload}
        }
        case SET_SECOND_GUARANT_TELEPHONE: {
            return {...state, secondGuarantTelephone: action.payload}
        }
        case SET_SKYPE: {
            return {...state, skype: action.payload}
        }
        case SET_INCOME_PER_MONTH: {
            return {...state, incomePerMonth: action.payload}
        }
        case SET_HAS_OPENED_CREDITS: {
            return {...state, hasOpenedCredits: !state.hasOpenedCredits}
        }
        case SET_AMOUNT_PAY_MONTHLY_CREDITS: {
            return {...state, amountPayMonthlyCredits: action.payload}
        }
        case SET_HAS_OPENED_LOANS: {
            return {...state, hasOpenedLoans: !state.hasOpenedLoans}
        }
        case SET_AMOUNT_PAY_MONTHLY_LOANS: {
            return {...state, amountPayMonthlyLoans: action.payload}
        }
        case SET_AMOUNT_TOTAL_OTHER_OBLIGATIONS: {
            return {...state, amountTotalOtherObligations: action.payload}
        }
        case SET_IS_PENSIONER: {
            return {...state, isPensioner: !state.isPensioner}
        }
        case SET_IS_UNEMPLOYED: {
            return {...state, isUnemployed: !state.isUnemployed}
        }
        case SET_WORK_NAME: {
            return {...state, workName: action.payload}
        }
        case SET_POSITION_NAME: {
            return {...state, positionName: action.payload}
        }
        case SET_WORK_ADDRESS: {
            return {...state, workAddress: action.payload}
        }
        case SET_PHONE_WORKS: {
            return {...state, phoneWork: action.payload}
        }
        case SET_WAS_DECLARED_AS_BANKRUPT: {
            return {...state, wasDeclaredAsBankrupt: !state.wasDeclaredAsBankrupt}
        }
        case SET_AGREE_WITH_DDO: {
            return {...state, agreedWithDdo: !state.agreedWithDdo}
        }
        case SET_AGREE_WITH_BKI: {
            return {...state, agreedWithBki: !state.agreedWithBki}
        }
        case SET_DATA_STEP3: {
            return {...state, stepThreeDataReturn: action.payload, code: action.payload.code}
        }
        case SET_STEP3_ERRORS: {
            return {...state, step3Errors: action.payload.errors}
        }
        case CLEAR_ERROR: {
            return {...state, step3Errors: {...state.step3Errors, [action.payload]: ""}}
        }
    }
    return state;
};
