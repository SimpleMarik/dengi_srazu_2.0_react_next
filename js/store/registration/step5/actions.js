import {
  RUN_TEST_STEP5, SET_CARD_MONTH, SET_CARD_NUMBER, SET_CARD_YEAR, SET_DATA_STEP5, SET_MONEY_CHANNEL_ID,
} from "../../constants";
// import {registerStepFive} from "../../../services/Api";
import Router from "next/router";

export const runTestStep5 = () => {
    return {
        type: RUN_TEST_STEP5,
    }
};

export const setStep5 = (data) => {
    return {
        type: SET_DATA_STEP5,
        payload: data,
    }
};

export const setCardNumber = (number) => {
    return {
        type: SET_CARD_NUMBER,
        payload: number,
    }
};
export const setCardMonth= (month) => {
    return {
        type: SET_CARD_MONTH,
        payload: month,
    }
};

export const setcardsYear = (year) => {
    return {
        type: SET_CARD_YEAR,
        payload: year
    }
};

export const setMoneyChannelId = (selected) => {
    return {
        type: SET_MONEY_CHANNEL_ID,
        payload: selected.target.getAttribute("channel-id"),
    }
};
export const submitStepFive = () => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep5Reducer;
        const goTest = registration.regStep5Reducer.goTest;
        const extId = registration.regStep4Reducer.stepFourDataReturn.extId;
        const testData = data.testData;
        console.log(extId);
        const registerData = {
            extId: extId,
            cardNumber: goTest ? testData.card.cardNumber : data.card.cardNumber,
            cardMonth: goTest ? testData.card.cardMonth : data.card.cardMonth,
            cardYear: goTest ? testData.card.cardYear : data.card.cardYear,
            moneyChannelId: goTest ? testData.card.moneyChannelId : data.card.moneyChannelId,
        };
        // registerStepFive(registerData).then(function (r) {
        //     if (r.data) {
        //         dispatch(setStep5(r.data));
        //         Router.push("/six");
        //     }
        // });
    }
};
