import {
    CLEAR_ERROR, RUN_TEST_STEP5, SET_DATA_STEP6,
    SET_PHOTO_PASSPORT, SET_PHOTO_PASSPORT_REG, SET_PHOTO_PASSPORT_REG_URL, SET_PHOTO_PASSPORT_URL,
    SET_PHOTO_WITH_CODE,
    SET_PHOTO_WITH_CODE_URL,SET_STEP6_ERRORS
} from "../../constants";
// import {registerStepSix, uploadPhoto} from "../../../services/Api";
import Router from "next/router";

export const runTestStep5 = () => {
    return {
        type: RUN_TEST_STEP5,
    }
};

export const setPhotoWithCodeUrl = (photoWithCodeUrl) => {
    return {
        type: SET_PHOTO_WITH_CODE_URL,
        payload: photoWithCodeUrl,
    }
};

export const setPhotoWithCode = (file) => {
    return {
        type: SET_PHOTO_WITH_CODE,
        payload: file,
    }
};

export const setPhotoPassportnUrl = (photoWithCodeUrl) => {
    return {
        type: SET_PHOTO_PASSPORT_URL,
        payload: photoWithCodeUrl,
    }
};

export const setPhotoPassport = (file) => {
    return {
        type: SET_PHOTO_PASSPORT,
        payload: file,
    }
};
export const setPhotoPassportRegUrl = (photoWithCodeUrl) => {
    return {
        type: SET_PHOTO_PASSPORT_REG_URL,
        payload: photoWithCodeUrl,
    }
};

export const setPhotoPassportReg = (file) => {
    return {
        type: SET_PHOTO_PASSPORT_REG,
        payload: file,
    }
};

export const setStep6 = (data) => {
    return {
        type: SET_DATA_STEP6,
        payload: data,
    }
};
export const setStep6Errors = (errors) => {
    return {
        type: SET_STEP6_ERRORS,
        payload: errors
    }
};

export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};
export const setUploadPhotoWithCode = (e) => {
    return (dispatch) => {
        console.log(e);
        if (!e.dataTransfer) {
            const reader = new FileReader();
            const file = e.target.files[0];
            reader.onloadend = () => {
                dispatch(setPhotoWithCode(reader.result));
            };
            reader.readAsDataURL(file);
            const formData = new FormData();
            formData.append("file", e.target.files[0]);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoWithCodeUrl(r.data.path));
            // })
        } else {
            let dt = e.dataTransfer;
            const dropFile = dt.files[0];
            const reader = new FileReader();
            reader.onloadend = () => {
                dispatch(setPhotoWithCode(reader.result));
            };
            reader.readAsDataURL(dropFile);
            const formData = new FormData();
            formData.append("file",dropFile);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoWithCodeUrl(r.data.path));
            // });
            console.log(dropFile);
        }
    }
};
export const setUploadPhotoPassport = (e) => {
    return (dispatch) => {
        if (!e.dataTransfer) {
            const formData = new FormData();
            let reader = new FileReader();
            let file = e.target.files[0];
            reader.onloadend = () => {
                dispatch(setPhotoPassport(reader.result));
            };
            reader.readAsDataURL(file);
            formData.append("file", e.target.files[0]);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoPassportnUrl(r.data.path));
            // })
        } else {
            let dt = e.dataTransfer;
            const dropFile = dt.files[0];
            const reader = new FileReader();
            reader.onloadend = () => {
                dispatch(setPhotoPassport(reader.result));
            };
            reader.readAsDataURL(dropFile);
            const formData = new FormData();
            formData.append("file",dropFile);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoPassportnUrl(r.data.path));
            // });
            console.log(dropFile);
        }
    }
};
export const setUploadPhotoPassportReg = (e) => {
    return (dispatch) => {
        if (!e.dataTransfer) {
            const formData = new FormData();
            let reader = new FileReader();
            let file = e.target.files[0];
            reader.onloadend = () => {
                dispatch(setPhotoPassportReg(reader.result));
            };
            reader.readAsDataURL(file);
            formData.append("file", e.target.files[0]);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoPassportRegUrl(r.data.path));
            // })
        } else {
            let dt = e.dataTransfer;
            const dropFile = dt.files[0];
            const reader = new FileReader();
            reader.onloadend = () => {
                dispatch(setPhotoPassportReg(reader.result));
            };
            reader.readAsDataURL(dropFile);
            const formData = new FormData();
            formData.append("file",dropFile);
            // uploadPhoto(formData).then(function (r) {
            //     dispatch(setPhotoPassportRegUrl(r.data.path));
            // });
            console.log(dropFile);
        }
    }
};

export const submitStepSix = (history) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep6Reducer;
        const extId = registration.regStep5Reducer.stepFiveDataReturn.extId;
        const registerData = {
            extId: extId,
            photoWithCode: data.photoWithCodeUrl,
            photoPassport: data.photoPassportUrl,
            photoPassportReg: data.photoPassportRegUrl
        };
        // registerStepSix(registerData)
        //     .then(function (r) {
        //         if (r.data) {
        //             dispatch(setStep6(r.data));
        //             Router.push("/final-step");
        //         }
        //     })
        //     .catch((e) => {
        //         dispatch(setStep6Errors(e.response.data));
        //     })
    }
};
