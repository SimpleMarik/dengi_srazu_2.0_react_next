import {
    CLEAR_ERROR,
    SET_NEW_PASSWORD, SET_RESTORE_CODE, SET_RESTORE_ERRORS, SET_RESTORE_PASSWORD,
    SET_RESTORE_PASSWORD_STATUS, SET_RESTORE_PHONE, SET_RESTORE_SUCCES
} from "../constants";

export const setRestorePhone = (restorePhone) => {
    return {
        type: SET_RESTORE_PHONE,
        payload: restorePhone,
    }
};

export const setNewPassword = (newPassword) => {
    return {
        type: SET_NEW_PASSWORD,
        payload: newPassword
    }
};

export const setRestoreCode = (restoreCode) => {
    return {
        type: SET_RESTORE_CODE,
        payload: restoreCode
    }
};

export const setRestorePassword = (restorePassword) => {
    return {
        type: SET_RESTORE_PASSWORD,
        payload: restorePassword
    }
};

// export const setRestoreCodeResent= (restoreCodeResent) => {
//     return {
//         type: SET_RESTORE_CODE_RESENT,
//         payload: restoreCodeResent
//     }
// };
export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

export const setRestoreSucces = (succes) => {
    return {
        type: SET_RESTORE_SUCCES,
        payload: succes
    }
};

export const setRestoreErrors = (restoreErrors) => {
    return {
        type: SET_RESTORE_ERRORS,
        payload: restoreErrors
    }
};


