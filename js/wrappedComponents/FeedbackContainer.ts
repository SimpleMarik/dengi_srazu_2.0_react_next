import {connect} from 'react-redux';
import Feedback from "../components/cabinet/Feedback";
import {addFile, addInputField} from "../store/cabinet/feedback/actions";

const mapDispatchToProps = {
    addFile,
    addInputField
};
export default connect(state=>state,mapDispatchToProps)(Feedback);
