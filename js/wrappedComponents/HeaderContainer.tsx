import {connect} from 'react-redux'
import Header from "../components/Header";
import {_setOpenMenu} from "../store/header/actions";
import { test} from "../store/actions";

const mapDispatchToProps = {
    _setOpenMenu,
    test,
};
export default connect(state=>state,mapDispatchToProps)(Header);
