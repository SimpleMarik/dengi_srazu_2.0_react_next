import {connect} from 'react-redux';
import LoanHistory from "../components/cabinet/LoanHistory";
// import {apiClientGetContracts} from "../services/Api";
import {getAgreements, getSelectedAgreements} from "../store/cabinet/agreements/actions";

const mapDispatchToProps = {
    // getUserLoan
    getAgreements,
    getSelectedAgreements
};
export default connect(state=>state,mapDispatchToProps)(LoanHistory);
