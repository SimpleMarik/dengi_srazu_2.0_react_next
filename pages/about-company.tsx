import React from "react";
import Slider from "react-slick";
import "../scss/components/about-company.scss"
import Pages from "../js/layouts/Pages";
import {slidersSettings} from "../js/helpers/helpers";

export default function AboutCompany() {
    const settings = slidersSettings(1, 5);
    return (
        <Pages>
            <section className="about-company">
                <div className="about-company__wrapper">
                    <h1 className="about-company__title">О компании</h1>
                    <p className="about-company__title-text">
                        Признание коллег и доверие клиентов вдохновляют компанию
                        на достижение новых высот. сегодня &laquo;Деньги Сразу&raquo; один из лидеров среди российских
                        микрофинансовых
                        организаций по общему размеру портфеля
                    </p>
                    <ul className="about-company__advantages-list">
                        <li className="about-company__advantages-item">
                            <p className="about-company__advantages-item-title">
                                280
                            </p>
                            <p className="about-company__advantages-item-text">
                                центров выдачи займов
                            </p>
                        </li>
                        <li className="about-company__advantages-item">
                            <p className="about-company__advantages-item-title">
                                134
                            </p>
                            <p className="about-company__advantages-item-text">
                                населенных пунктов присутствия
                            </p>
                        </li>
                        <li className="about-company__advantages-item">
                            <p className="about-company__advantages-item-title">
                                920
                            </p>
                            <p className="about-company__advantages-item-text">
                                тысяч клиентов компании
                            </p>
                        </li>
                    </ul>
                    <p className="about-company__img-wrapper">
                        <img src="/static/images/about-company/map.png" alt="about map"/>
                    </p>
                    <h2 className="about-company__advantages-title">
                        &laquo;Деньги сразу&raquo; - это
                    </h2>
                    <p className="about-company__advantages-title-text">
                        Главная цель &laquo;Деньги сразу&raquo; - обеспечить заемщику спокойствие и финансовую
                        потдержку,
                        по этому мы предоставляем полный доступ к нашей документации. На нас можно положиться и нам
                        можно доверять. Деятельность &laquo;Деньги сразу&raquo; основана на оперативности, лояльности
                        и честности по отношению к клиентам.
                    </p>
                    <ul className="about-company__advantages-next-list">
                        <li className="about-company__advantages-next-item">
                            <h3 className="about-company__advantages-next-item-title">
                                Оперативность
                            </h3>
                            <p className="about-company__advantages-next-item-text">
                                Люди, обращающиеся за финансовой помощью, как правило, нуждаются в не &laquo;здесь и
                                сейчас&raquo;. менно по
                                этому срок рассмотрения каждой заявки и выдача кредита в &laquo;Деньги сразу&raquo; не
                                занимает более 20 минут.
                            </p>
                        </li>
                        <li className="about-company__advantages-next-item">
                            <h3 className="about-company__advantages-next-item-title">
                                Лояльность
                            </h3>
                            <p className="about-company__advantages-next-item-text">
                                В ситуации, когда срочно нужны деньги, может оказаться любой человек. По этой причине в
                                офисах
                                &laquo;Деньги сразу&raquo; каждому клиенту предъявляются минимальные требования. Все,
                                что
                                нужно для получения
                                займа - это пасспорт гражданина РФ и личное присутствие.
                            </p>
                        </li>
                        <li className="about-company__advantages-next-item">
                            <h3 className="about-company__advantages-next-item-title">
                                Честность
                            </h3>
                            <p className="about-company__advantages-next-item-text">
                                Мы не используем скрытых комиссий, штрафов и пени, что помогает нам выстраивать
                                абсолютно
                                прозрачные отношения
                                в своей работе с клиентом. Наша цель - помочь человеку в трудной финансовой ситуации.
                            </p>
                        </li>
                    </ul>
                    <div className="about-company__corporate-symbols-wrapper">
                        <div className="about-company__corporate-symbols-img-wrapper">
                            <img src="/static/images/about-company/simbolick.png" alt="corporate image"/>
                        </div>
                        <div className="about-company__corporate-symbols-text-wrapper">
                            <p className="about-company__corporate-symbols-text">
                                Корпоративная символика &laquo;Деньги сразу&raquo; является товарным знаком на
                                территории РФ
                            </p>
                            <p className="about-company__corporate-symbols-text">
                                Товарный знак
                            </p>
                        </div>
                    </div>
                    <div className="about-company__achievements">
                        <h2 className="about-company__achievements-title">
                            Достижения и награды
                        </h2>
                        <p className="about-company__achievements-text">
                            За все время работы компания &laquo;Деньги сразу&raquo; зарекомендовала себя как надежная
                            микрофинансовая
                            организация с высоким уровнем сервиса. Именно поэтому многие заемщики становятся нашими
                            постоянными клиентами.
                        </p>
                        <Slider {...settings}>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img1.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img2.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img3.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img4.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img5.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img6.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img7.jpg" alt="image"/>
                            </div>
                            <div className="about-company__slider-item">
                                <img src="/static/images/about-company/img8.jpg" alt="image"/>
                            </div>
                        </Slider>
                    </div>


                </div>
            </section>
        </Pages>
    )
}

