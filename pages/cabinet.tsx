import {connect} from 'react-redux'
import Cabinet from "../js/components/cabinet/Cabinet";
import {getOptions, isLogin, logoutUser} from "../js/store/auth/actions";

// import {getOptions} from "../js/store/cabinet/user-loan/reducers";


const mapDispatchToProps = {
    // getOptions,
    logoutUser,
    isLogin,
    getOptions
};


export default connect(state=>state, mapDispatchToProps)(Cabinet);
