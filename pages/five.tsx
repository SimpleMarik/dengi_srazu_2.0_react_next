import Five from "../js/components/registration/Five";
import {connect} from "react-redux";
import {
    runTestStep5,
    setCardMonth,
    setCardNumber,
    setcardsYear,
    setMoneyChannelId,submitStepFive
} from "../js/store/registration/step5/actions";

const mapDispatchToProps =
    {
        setCardNumber,
        setCardMonth,
        setcardsYear,
        submitStepFive,
        setMoneyChannelId,
        runTestStep5,
    };

export default connect(state=>state, mapDispatchToProps)(Five);
