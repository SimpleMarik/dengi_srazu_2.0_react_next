import {connect} from 'react-redux'
import Login from "../js/components/Login";
import {
    setAuthCode,
    setAuthPassword,
    setAuthPhone,
    clearError
} from "../js/store/auth/actions";
import {submitAuth} from "../js/store/auth/actions";

const mapDispatchToProps = {
    setAuthPhone,
    setAuthCode,
    submitAuth,
    clearError,
    setAuthPassword
};

export default connect(state=>state, mapDispatchToProps)(Login);
