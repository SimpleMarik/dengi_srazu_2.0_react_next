// import React from "react";
import One from "../js/components/registration/One";
import {connect} from "react-redux";
import {
    setPhone,
    setName,
    setPatronymic,
    setSecondName,
    setGender,
    setBirthDate,
    setSubscribe,
    setConsentPersonalData, runTest, clearError, getRegData

} from "../js/store/registration/step1/actions";
import {submitStepOne} from "../js/store/registration/step1/actions";
// import {withRouter} from "next/router";


const mapDispatchToProps =
    {
        setSecondName,
        setName,
        setPatronymic,
        setBirthDate,
        setGender,
        setPhone,
        submitStepOne,
        setSubscribe,
        setConsentPersonalData,
        runTest,
        clearError,

        getRegData
    };

export default  connect(state=>state, mapDispatchToProps)(One);
