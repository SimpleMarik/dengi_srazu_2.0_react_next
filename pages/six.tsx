import Six from "../js/components/registration/Six";
import {connect} from "react-redux";
import {
    setUploadPhotoPassport, setUploadPhotoPassportReg,
    setUploadPhotoWithCode,
    submitStepSix,clearError
} from "../js/store/registration/step6/actions";

const mapDispatchToProps =
    {
        setUploadPhotoWithCode,
        setUploadPhotoPassport,
        setUploadPhotoPassportReg,
        submitStepSix,
        clearError
    };

export default connect(state => state, mapDispatchToProps)(Six);
