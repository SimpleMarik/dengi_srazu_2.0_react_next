// import {createProxyMiddleware} from "http-proxy-middleware";

const next = require('next');
const cookieParser = require('cookie-parser');
const port = parseInt(process.env.PORT, 10) || 3001;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
const express = require("express");
const compression = require('compression');
// const proxy = require('http-proxy-middleware');

app.prepare().then(() => {
    const server = express();
    // const exampleProxy = proxy(options)
    server.use(cookieParser());
    server.use(compression());
// проксирование
    const { createProxyMiddleware } = require('http-proxy-middleware')
    // const apiPaths = {
    //     '/ds/v1': {
    //         target: 'http://rc.webdb.dengisrazy.ru',
    //         pathRewrite: {
    //             '^/ds/v1': '/ds/v1'
    //         },
    //         changeOrigin: true
    //     }
    // }
    // server.use('/ds/v1', createProxyMiddleware(apiPaths['/ds/v1']));


    // server.use('/api', createProxyMiddleware({target: '/fdsfs', changeOrigin: true}))
    server.get('*', (req, res) => {
        return handle(req, res);

    });

    server.listen(port, err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`)
    });
});
